import sys
from sonickit import Runner, level, store, builtins, boss, logging, resources
from levelgen import heuristics, structure, layout, implementation
from ghz import objects, style
from sonickit.players.sonic import Sonic

runner = Runner()
resources.load_global()

if len(sys.argv) == 1:
	print("Usage: script.py <num-levels>")

num_levels = int(sys.argv[1])

profile = heuristics.PlayerProfile(learning_rate=0.4)
player = Sonic()

def generate_level(act_number, num_levels, profile):
	"""Generate a new level with weighting probabilities set according to the given player profile."""
	e = profile.exploration_tendency
	r = profile.rush_tendency
	graph, start = structure.generate_structure_graph(
	     split_chance=e * 0.3,
	     merge_chance=e * 0.15,
		 dead_end_chance=e * 0.5,
	     max_branching_factor=2,
		 max_parallel_paths=4,
		 target_size=16,
		 node_types={structure.BonusArea: 2, structure.ChallengeArea: 1 + profile.skill * 3})
	
	blocks, adjacencies = layout.generate_block_layout(
	     graph, start,
	     left_weight=1 + e * 3 - r,
		 right_weight=20 + r * 80,
		 up_weight=3 + e * 2 - r * 2,
		 down_weight=1 + r * 4)
	
	monitor_weights = profile.monitor_preference
	# Use the pre-defined implementation table as a template, modifying its values
	table = {
		structure.StartPoint: (objects.build_start_node, {}),
		structure.InterstitialNode: (objects.build_interstitial_node, {"ground_rings_chance": profile.ring_valuation}),
		structure.EndPoint: (objects.build_final_node, {"boss_difficulty_level": round(1 + profile.skill * 3), "boss_chance": 1}),
		structure.BonusArea: (objects.build_bonus_area, 
		     {"shield_weight": monitor_weights[builtins.ShieldMonitor],
			 # Give a little more life monitors if the player is having trouble
			  "life_weight": monitor_weights[builtins.LifeMonitor] + 5 * (1 - profile.skill),
		      "ring_weight": monitor_weights[builtins.RingMonitor] + 5 * profile.ring_valuation}),
		structure.ChallengeArea: (objects.build_challenge_area, {"enemies_chance": profile.skill, "moving_platforms_chance": (e + profile.skill) / 2, "loop_chance": 0.15 + 0.85 * (1 - profile.skill)})
	}
	
	level = implementation.generate_level(
	     blocks, adjacencies,
		 table,
		 block_width=256, block_height=256,
		 background=style.GHZBackground(), background_music=style.background_music)
		 
	# Clear the log
	logging.clear_log()
	# Set these manually
	level.act_number = act_number
	if act_number < num_levels:
		def on_end():
			profile.update()
			start_level(generate_level(act_number + 1, num_levels, profile))
	else:
		on_end = exit
	level.on_end = on_end
	return level

def start_level(level):
	level.place_player(player)
	runner.current_section = level

start_level(generate_level(1, num_levels, profile))
runner.run()