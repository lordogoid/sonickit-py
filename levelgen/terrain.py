import math
import random
from sonickit import object as sk_object
from pygame import Surface, SRCALPHA, draw

# The lowest height to which a height map may fall.
HEIGHT_MAP_BOTTOM = 32

class HeightMap:
	"""A height map is a description of the surface of a platform, in the form of a piecewise-defined function giving the height at a particular X coordinate. This can be used
	to build the collision map for the corresponding platform, by calling the object with the X
	coordinate (relative to the start of the height map) to get the corresponding height.
	
	Additionally, height maps have a width and maximum_height, which is the height of the highest point of the platform, as well as starting_height and final_height attributes: the former
	is the starting height passed to the constructor and the latter is the height of the right end
	of the last segment.
	"""
	def __init__(self, width, starting_height, height_iterator, max_segments=1, flat_weight=1, ramp_weight=1, slope_weight=1, discontinuity_probability=0):
		# This contains the list of functions that generate the height of each segment at a given X position
		self.segments = []
		self.segment_widths = []
		# This is needed so that range(width) is valid; the ceiling is used so that
		# all platforms are at least as large as requested
		self.width = math.ceil(width)
		num_segments = random.randint(1, max_segments)
		total_weight = flat_weight + ramp_weight + slope_weight
		current_height = starting_height
		maximum_height = starting_height
		self.starting_height = starting_height
		# Prevent the height from falling to very low values (to avoid rendering glitches when using the height map to render platforms)
		original_height_iterator = height_iterator
		height_iterator = lambda h: max(original_height_iterator(h), HEIGHT_MAP_BOTTOM)
		for i in range(num_segments):
			# Introduce discontinuities (side ledges) randomly
			if random.random() < discontinuity_probability:
				current_height = height_iterator(current_height)
				maximum_height = max(maximum_height, current_height)
			# Choose a type of segment at random
			pick = random.uniform(0, total_weight)
			segment_width = width / num_segments
			next_height = height_iterator(current_height)
			x_offset = sum(self.segment_widths)
			if pick < flat_weight:
				self.segments.append(flat_ground(current_height))
				next_height = current_height
			elif pick < flat_weight + ramp_weight:
				self.segments.append(linear_ground(current_height, next_height, segment_width, x_offset))
			else:
				self.segments.append(sigmoid_ground(current_height, next_height, segment_width, x_offset))
			self.segment_widths.append(segment_width)
			current_height = next_height
			maximum_height = max(maximum_height, current_height)
		self.final_height = current_height
		self.maximum_height = maximum_height
	
	def __call__(self, x):
		"""Returns the height of the height map at the given X coordinate. Raises an exception
		if that is out of range."""
		if not 0 <= x <= self.width:
			raise IndexError("X coordinate {} out of range".format(x))
		accum_width = 0
		for segment, width in zip(self.segments, self.segment_widths):
			accum_width += width
			if x <= accum_width:
				return segment(x)
		return self.segments[-1](self.width - 1)

def flat_ground(height):
	return lambda x: height

def linear_ground(start_height, end_height, width, x_offset):
	slope = (end_height - start_height) / width
	return lambda x: start_height + slope * (x - x_offset)

# The value of x above which erf(x) no longer increases visibly (used to construct erf() shaped
# terrain)
ERF_CONVERGENCE_POINT = 2
ERF_LIMIT = math.erf(ERF_CONVERGENCE_POINT) + 1

def sigmoid_ground(start_height, end_height, width, x_offset):
	diff = end_height - start_height
	k = 2 * ERF_CONVERGENCE_POINT / width
	return lambda x: start_height + (math.erf((x - x_offset) * k - ERF_CONVERGENCE_POINT) + 1) / ERF_LIMIT * diff
	
class QuarterPipeHeightMap:
	def __init__(self, width, height, starting_height, horizontal_offset, point_left):
		"""Creates a height map describing a quarter-pipe. The curve, which
		is circular, is cut out of a rectangle with the specified dimensions.
		
		initial_height is the height of the horizontal segment of the quarter-pipe;
		horizontal_offset is the distance from the solid edge of the pipe to the top end of
		the curve.
		
		The curve may face towards the left or the right, as dictated by the point_left
		argument."""
		self.starting_height = starting_height
		self.width = width
		self.maximum_height = height
		self.final_height = height
		self.horizontal_offset = horizontal_offset
		self.curve_radius = min(height - starting_height, width - horizontal_offset)
		self.straight_section_width = max(0, width - horizontal_offset - self.curve_radius)
		self.point_left = point_left
	
	def __call__(self, x):
		if not 0 <= x <= self.width:
			raise IndexError("X coordinate {} out of range".format(x))
		if not self.point_left:
			x = self.width - x
		if x < self.straight_section_width:
			return self.starting_height
		if x < self.width - self.horizontal_offset:
			k = 1 - math.sqrt(max(0, 1 - ((x - self.straight_section_width) / self.curve_radius) ** 2))
			return self.starting_height + k * self.curve_radius
		return self.maximum_height
			
class HeightMappedPlatform(sk_object.Object):
	def __init__(self, x, y, layer, height_map, top_margin=0):
		self.height_map = height_map
		self.collision_map = generate_collision_map(height_map, top_margin)
		self.image = self.collision_map
		super().__init__(x, y, layer)
		
	@property
	def width(self):
		return self.height_map.width
	
	@property
	def height(self):
		return self.collision_map.get_height()
	
	@property
	def collision_mask(self):
		return self.collision_map
	
	pixel_distance = sk_object.SpriteObject.pixel_distance
	vertical_distance = sk_object.SpriteObject.vertical_distance
	horizontal_distance = sk_object.SpriteObject.horizontal_distance
		
	ignore_physics = False
	top_solid = True
	bottom_solid = left_solid = right_solid = True
	
	# Shorthand required by the physics sensors' code
	hd = horizontal_distance
	vd = vertical_distance
	
	def draw(self, surface, camera, rem_time):
		surface.blit(self.image, (self.rect.left - camera.x, self.rect.top - camera.y))
	
	def place_object_atop(self, object, x=None, y_offset=0):
		"""Sets the given object's coordinates such that it is at the specified X coordinate and its bottom side is y_offset pixels above the top of this platform at that X coordinate. If no X coordinate is specified, it is chosen randomly."""
		if x is None:
			x = random.uniform(self.rect.left, self.rect.right)
		object.x = x
		object.y = self.rect.bottom - self.height_map(x - self.rect.left) - object.rect.height / 2 - y_offset

def tile(surface, pattern):
	"""Fills surface with tiled copies of the given pattern."""
	# The number of times to repeat the pattern in either direction
	w = pattern.get_width()
	h = pattern.get_height()
	nx = math.ceil(surface.get_width() / w)
	ny = math.ceil(surface.get_height() / h)
	for x in range(nx):
		for y in range(ny):
			surface.blit(pattern, (x * w, y * h))

def generate_collision_map(height_map, top_margin=0):
	"""Returns an image (as a pygame.Surface) containing a filled shape with the left, right and bottom sides straight and the top side in the shape defined by given height map.
	If top_margin is nonzero, that many blank rows are added at the top; otherwise, the resulting image is the minimum size
	needed to fit the shape."""
	image = Surface((height_map.width, height_map.maximum_height + top_margin), flags=SRCALPHA)
	image.lock()
	for x in range(height_map.width):
		height = height_map(x)
		draw.line(image, (0, 0, 0), (x, image.get_height()), (x, image.get_height() - height))
	image.unlock()
	return image