"""This module is responsible for taking the block-mapping of the nodes representing the level's 
structure, and producing an actual level by filling in the (rectangular) blocks. To do this, it 
defers to a set of implementations corresponding to each block type.

The implementations are functions which take as parameters:
- the level object;
- the rectangular area to place objects in;
- whether there is an edge from the block to each adjacent block ("has_neighbor");
- whether the block has an adjacent block at all in each direction ("has_adjacent");
- when applicable, the lower and upper limits (the "exits") of the connections with said neighboring blocks.
  (None if not defined so far or not applicable)"""

from . import layout
from .aux import all_directions
from pygame import Rect
from sonickit.level import Level
from sonickit.background import Background

def generate_level(block_map, block_adjacencies, implementation_table, block_width=256, block_height=256, background=Background(color=(222, 222, 222)), background_music=None):
	"""Constructs and returns a SonicKit level based on a mapping from logical blocks to nodes of
	the types used to build a structure graph, and the information on the connections between
	them, such as the ones returned by layout.generate_block_layout.
	
	This function takes as a parameter an "implementation table": a mapping from
	node classes to a tuple containing:
	- a function used to construct the portion of the level mapped to the nodes.
	- a dictionary containing any additional arguments to pass to the function (for example,
      probabilities for various design choices)
	  
	Additionally, a background and background music may be specified. Otherwise, the same defaults
	are used as in the Level constructor.
	
	This function also allows the specification of the size of each block; by default, it is
	set to 256 by 256 pixels (the size of terrain blocks in the original Sonic the Hedgehog)."""
	min_x, max_x, min_y, max_y = layout.bounding_box(block_map)
	level_width = max_x - min_x + 1
	level_height = max_y - min_y + 1
	level = Level(background=background, background_music=background_music, bounding_box=Rect(min_x * block_width, min_y * block_height, level_width * block_width, level_height * block_height))
	# For each block (identified by its coordinates), this contains a dictionary containing
	# the upper and lower limits of its exit towards each of its neighbors, along the
	# appropriate edge of that block and relative to its bounding box.
	block_exits = {}
	for block in block_map:
		i, j = block
		node = block_map[block]
		# Check if the block has any adjacent blocks. If it does, and these blocks have already
		# been constructed, then look up their exits toward this block
		neighbors = all_directions(block)
		has_neighbors = [n in block_adjacencies[block] for n in neighbors]
		has_adjacent = [n in block_map for n in neighbors]
		
		# block_exits[n][block] is always valid, because each value of block_exits contains an
		# entry for all four neighbors, guaranteed by the last line of this loop.
		exits = [block_exits[n][block] if n in block_exits and n in block_adjacencies[block] else None for n in neighbors]
		implementation, kwargs = implementation_table[type(node)]
		new_exits = implementation(level, area=Rect(i * block_width, j * block_height, block_width, block_height), has_neighbors=has_neighbors, has_adjacent=has_adjacent, exits=exits, **kwargs)
		block_exits[block] = dict(zip(neighbors, new_exits))
	return level