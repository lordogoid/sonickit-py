import random
import itertools
import bisect
from sonickit.level import Level

class WeightedChoice:
	def __init__(self, choice_table):
		self.choices = [choice for choice, weight in choice_table]
		self.cumulative_weights = list(itertools.accumulate(weight for choice, weight in choice_table))
	
	def choice(self):
		pick = random.uniform(0, self.cumulative_weights[-1])
		return self.choices[bisect.bisect(self.cumulative_weights, pick)]
		
BRANCH_DIRECTIONS = frozenset(("UP", "DOWN"))

def generate_level(structure_graph, start_node, implementations):
	"""Takes a structure graph (such as one obtained from structure.generate_structure_graph)
	and returns a level that matches that structure. The structure graph should be a MultiDiGraph (see NetworkX documentation for more details), and start_node should be the initial node of the level.
	
	The implementations parameter should be a dictionary whose keys are the classes of nodes
	in the graph, and the values are lists of functions and the corresponding weights (the probability of any one function being chosen is that function's weight divided by the sum of weights for that node).
	In addition, it is necessary to include extra keys to support the construction of branches
	and connecting platforms (which correspond to the edges of the graph).
	The table should contain keys for all classes of node found in the graph; otherwise, this
	function will raise a KeyError.
	A valid implementations table looks like the following:
	
	{structure.GenericPlace: [(item_box, 1), (nothing_but_decoration, 3)],
     "RIGHT_CONNECTOR": [(boring_platform, 1)]}"""
	level = Level()
	visited_nodes = set()
	# Replace the lists of choices with the weighted choice helpers
	implementations = {key: WeightedChoice(value) for key, value in implementations.items()}
	# The coordinates of the left-most and right-most end of the ground corresponding to each node
	# (or pair of adjacent nodes, for the case of connecting platforms)
	left_ends = {}
	right_ends = {}
	# For each node, this dictionary contains a tuple whose elements are:
	# - a list of the platforms in the connecting segment after that node
	# - the set of directions of the alternative paths (beyond the first found) from the node, if any (a subset of BRANCH_DIRECTIONS)
	branches = {}
	# Repeat until all nodes in the graph have been visited
	while len(visited_nodes) < len(structure_graph):
		print("Nodes visited: {}/{}".format(len(visited_nodes), len(structure_graph)))
		node = start_node
		insertion_point = None
		while True:
			if node not in visited_nodes:
				if node is start_node:
					insertion_point = (0, 0)
				implementation = implementations[type(node)].choice()
				# Execute the chosen implementation for this node, and advance the insertion point
				left_ends[node] = insertion_point
				insertion_point, objects = implementation(level, insertion_point)
				right_ends[node] = insertion_point
			else:
				insertion_point = right_ends[node]
			# Choose a random successor to this node, among those not already visited if possible
			successors = structure_graph.successors(node)
			# If this the end node (which has no successors), the current path is complete.
			if not successors:
				visited_nodes.add(node)
				print("Completed a path!")
				break
			unvisited = list(filter(lambda s: s not in visited_nodes, successors))
			next_node = random.choice(unvisited if unvisited else successors)
			# If no successor has yet been visited, this is the first path out of this node.
			# Build a new connecting platform to the right.
			if len(unvisited) == len(successors):
				implementation = implementations["RIGHT_CONNECTOR"].choice()
				left_ends[(node, next_node)] = insertion_point
				insertion_point, objects = implementation(level, insertion_point)
				right_ends[(node, next_node)] = insertion_point
				branches[node] = (objects, set())
			# If some, but not all successors have been visited, turn the connecting platform to the right into a branch towards the top or the bottom (or both),
			# by choosing a direction that has not been used yet at this node.
			elif unvisited:
				current_direction_set = branches[node][0]
				try:
					branch_direction = random.choice(list(BRANCH_DIRECTIONS - current_direction_set))
				except IndexError as err:
					raise NotImplemented("{}-way branch found; available branch directions only allow at most {}-way branches".format(len(successors) + 1), len(BRANCH_DIRECTIONS)) from err
				current_direction_set.add(branch_direction)
				branch_type_name = "_".join(sorted(current_direction_set)) + "_BRANCH"
				implementation = implementations[branch_type_name].choice()
				# Pass the existing objects in the connection to the implementation function,
				# so that it can remove or modify them
				left_ends[(node, next_node)] = insertion_point
				insertion_point, objects = implementation(level, insertion_point, branches[node][1])
				right_ends[(node, next_node)] = insertion_point
				branches[node][1] = objects
			else:
				# We have chosen an already-visited node. Advance the insertion point, then skip ahead.
				insertion_point = right_ends[(node, next_node)]
			# Mark the just-processed node as visited and proceed to the next one.
			visited_nodes.add(node)
			node = next_node
	return level
			