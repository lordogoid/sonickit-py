import random
import sys
import networkx as nx
from .aux import WeightedChoice

node_seq_number = 0

class Node:
	def __init__(self):
		self.id = random.randint(0, sys.maxsize)
		global node_seq_number
		self.seq_number = node_seq_number
		node_seq_number += 1
	
	def __hash__(self):
		return self.id
	
	def __repr__(self):
		return "<{}: {}>".format(type(self).__qualname__, self.seq_number)

class StartPoint(Node):
	pass

class InterstitialNode(Node):
	pass
	
class ChallengeArea(Node):
	pass

class BonusArea(Node):
	pass

class EndPoint(Node):
	pass

def generate_structure_graph(split_chance, merge_chance, dead_end_chance, max_branching_factor, max_parallel_paths, target_size, node_types):
	"""Generates a random level structure graph. The graph is a directed acyclic graph (DAG) with potentially several parallel paths.
	
	split_chance is the probability of, at each step, a random path being split into a set of new paths.
	max_branching_factor is the maximum number of branches that may spawn from a split path
	(the number of branches is determined at random up to that number.)
	max_parallel_paths is the maximum number of separate paths that may exist at any one time.
	
	dead_end_chance is the probability of, at each step, a random path ending without merging onto another. This does not occur if there is only one path in that iteration.
	
	merge_chance is the probability, at each step and for each path, of that path merging onto another random path (if there is one available).
	
	target_size is the number of nodes beyond which the generator will stop extending paths and end the level.
	
	node_types should be a dictionary mapping node types to the weights determining the 
	probability that they will be chosen for each node in the graph.
	
	This function returns a 2-tuple containing the graph and its initial node.
	"""
	graph = nx.MultiDiGraph()
	start = StartPoint()
	graph.add_node(start)
	node_types = WeightedChoice(node_types.items())
	# This list contains the last node for each path being built on each iteration
	paths = [start]
	new_paths = []
	while graph.number_of_nodes() < target_size:
		pick = random.random()
		if pick < split_chance and len(paths) < max_parallel_paths:
			# Prevent the number of paths from ever increasing past max_parallel_paths
			# Note that this "branching factor" is actually the branching factor minus 1,
			# to simplify the calculations
			branching_factor = random.randrange(1, max_branching_factor)
			branching_factor = min(branching_factor, max_parallel_paths - len(paths))
			# Choose a random node, then add duplicate references to it to the list of paths
			# (both paths will then be developed from the same node)
			paths.extend((random.choice(paths),) * branching_factor)
		elif pick >= split_chance and pick < split_chance + dead_end_chance and len(paths) > 1:
			# Remove a random path from the list; as a consequence, that path will not be processed further by the algorithm.
			del paths[random.randrange(len(paths))]
		updated_paths = []
		merged_paths = []
		for path in paths:
			will_merge = random.random() < merge_chance
			other_paths = [other_path for other_path in paths if other_path is not path and other_path not in merged_paths]
			if will_merge and other_paths:
				# Prevent the creation of self-loops in the graph
				merge_target = random.choice(other_paths)
				graph.add_edge(path, merge_target)
				# Do not add the path to the updated_paths list; merging it onto another path
				# effectively terminates it.
				# Mark it as merged, so that other paths do not merge onto it during this iteration of the algorithm.
				merged_paths.append(path)
			else:
				# Pick a node type at random
				next_node = node_types.choice()()
				graph.add_node(next_node)
				graph.add_edge(path, next_node)
				updated_paths.append(next_node)
		print
		paths = updated_paths
	# Join together all paths at the end
	end = EndPoint()
	graph.add_node(end)
	for path in paths:
		graph.add_edge(path, end)
	return (graph, start)

def draw_graph_to_file(graph, file):
	import matplotlib.pyplot as plt
	plt.clf()
	nx.draw_graphviz(graph, labels={node: str(node.seq_number) for node in graph.nodes_iter()}, with_labels=True, prog="dot")
	plt.savefig(file)