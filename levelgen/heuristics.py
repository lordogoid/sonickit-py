"""This module provides facilities to evaluate a player's playstyle based on statistics collected
during gameplay (see sonickit.logging)."""

import statistics
from sonickit import logging
from sonickit import builtins

def clamp_and_normalize(x, max_value):
	"""Clamps x to the range [0, max_value], then normalizes the result to the range [0, 1]."""
	return min(max(x, 0), max_value) / max_value
	
# The following functions compute each of the player behavior measures included in a PlayerProfile.

def rush_tendency(game_time, ground_speeds, spindash_values, roll_times, moving_platform_landings):
	mean_abs_speed = statistics.mean(map(abs, ground_speeds))
	spindashes_per_minute = len(spindash_values) / game_time
	rolls_per_minute = roll_times / game_time
	landings_per_minute = moving_platform_landings / game_time
	if spindash_values:
		mean_spindash_charges = statistics.mean(map(lambda entry: entry[1], spindash_values))
	else:
		mean_spindash_charges = 0
	x = (mean_abs_speed + spindashes_per_minute + rolls_per_minute + mean_spindash_charges - landings_per_minute) / 3
	return clamp_and_normalize(x, 10)
	
HIGH_JUMPS_PER_MINUTE = 20
HIGH_LANDINGS_PER_MINUTE = 4

def exploration_tendency(game_time, speeds, jumps, moving_platform_landings):
	# Compute the percentage of time on ground the player spends going left;
	# this assumes a primarily right-headed level (as is typical in Sonic games, but might not
	# happen using this generator, depending on the parameters chosen)
	left_heading_ticks = sum(1 for speed in speeds if speed < 0) / len(speeds)
	jumps_per_minute = jumps / game_time
	landings_per_minute = moving_platform_landings / game_time
	# Scale the latter two statistics to (approximately) [0, 1]
	x = (left_heading_ticks + (jumps_per_minute / HIGH_JUMPS_PER_MINUTE) + (landings_per_minute / HIGH_LANDINGS_PER_MINUTE)) / 3
	return clamp_and_normalize(x, 1)

def ring_valuation(old_ring_valuation, rings_collected, rings_spawned):
	if rings_spawned > 0:
		return rings_collected / rings_spawned
	else:
		return old_ring_valuation

def skill(game_time, deaths, hits):
	deaths_per_minute = deaths / game_time
	hits_per_minute = hits / game_time
	x = (deaths_per_minute + hits_per_minute) / 2
	return 1 - clamp_and_normalize(x, 5)

class PlayerProfile:
	"""A PlayerProfile object contains several descriptor values that aim to describe, approximately,
	the gameplay preferences and tendencies of a player. These are:
	
	- rush tendency: the player's tendency to go through levels quickly;
	- exploration tendency: the player's tendency to perform actions that do not directly
	advance towards the end of the level
	- ring valuation: measures how much the player cares about mantaining their ring count
	- skill: measures the player's ability to avoid taking damage and dying
	- monitor preference: how many monitors of each type does the player tend to collect.
	
	Except for monitor preference, all of these values range from 0 to 1."""
	def __init__(self, learning_rate=0.4):
		"""Creates a new player profile. Its values are initialized to the middle of the scale,
		and monitor preferences are set to be equal for all monitors.
		The learning rate determines how quickly the profile adjusts to gameplay data; however,
		it is not advised to use too high a value, as that will cause it to quickly "forget" about
		recent past tendencies."""
		self.rush_tendency = 0.5
		self.exploration_tendency = 0.5
		self.ring_valuation = 0.5
		self.monitor_preference = {builtins.RingMonitor: 1, builtins.LifeMonitor: 1, builtins.ShieldMonitor: 1}
		self.skill = 0.5
		self.learning_rate = learning_rate
	
	def update(self):
		"""Computes a new set of descriptor values based on the information stored in sonickit.logging; then mixes them with the existing values by the formula:
		
		result = old * (1 - r) + new * r,
		
		where r is the learning rate set in the constructor."""
		# Compute the game time (given in frames) into minutes
		time = logging.game_time / 3600
		new_rush_tendency = rush_tendency(time, logging.measurements["speed"], logging.measurements["spindash"], logging.counted_events["roll"], logging.counted_events["hit_moving_platform"])
		new_exploration_tendency = exploration_tendency(time, logging.measurements["speed"], logging.counted_events["jump"], logging.counted_events["hit_moving_platform"])
		new_ring_valuation = ring_valuation(self.ring_valuation, logging.counted_events["bouncing_rings_collected"], logging.counted_events["bouncing_rings_spawned"])
		new_skill = skill(time, logging.counted_events["player_died_damage"] + logging.counted_events["player_died_pit"], sum(logging.damage_taken.values()))
		
		new_monitor_preference = logging.hit_monitors
		# Mix these values ("new") with the "old" ones, taking into account the learning rate r -
		# mix = old * (1 - r) + new * r
		
		r = self.learning_rate
		s = 1 - r
		self.rush_tendency = self.rush_tendency * s + new_rush_tendency * r
		self.exploration_tendency = self.exploration_tendency * s + new_exploration_tendency * r
		self.ring_valuation = self.ring_valuation * s + new_ring_valuation * r
		self.skill = self.skill * s + new_skill * r
		# In the case of the monitor preference meters, mix them element by element 
		self.monitor_preference = {monitor_type: self.monitor_preference[monitor_type] * s + new_monitor_preference[monitor_type] * r for monitor_type in self.monitor_preference}