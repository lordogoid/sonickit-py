import random
import pdb
import itertools
from collections import defaultdict
from .aux import WeightedChoice, add, all_directions
from .structure import InterstitialNode

def pairs(sequence):
	"""Returns a sequence of the pairs of consecutive items in the given sequence."""
	return zip(itertools.islice(sequence, len(sequence) - 1), itertools.islice(sequence, 1, len(sequence)))
	
class LayoutError(Exception):
	pass

class DestinationUnreachable(LayoutError):
	pass

class DestinationNotReached(LayoutError):
	pass

class OutOfSpace(LayoutError):
	pass

class NotEnoughAttempts(Exception):
	pass

def retry(function, exception_type, max_tries=None):
	"""Attempts to call the given function up to max_tries times until it successfully returns without raising an exception of the specified type.
	Returns the return value of the first successful run of the function and the number of attempts made.
	
	If max_tries pass without any run of the function succeeding, this raises a NotEnoughAttempts exception."""
	num_tries = 0
	while max_tries is None or num_tries < max_tries:
		try:
			num_tries += 1
			return_value = function()
			return return_value, num_tries
		except exception_type:
			pass
	raise NotEnoughAttempts("{} attempts not enough".format(max_tries))

def generate_block_layout(structure_graph, start_node, left_weight, right_weight, up_weight, down_weight, max_tries=10):
	"""
	Maps a level structure graph onto a grid of fixed-size blocks. Each block is occupied by
	one and only one node, but nodes may potentially use more than one block. Blocks may have 
	edges to each of their four direct neighbors.
	
	Returns:
	- a dictionary mapping each node to the set of level blocks it occupies
	- another dictionary indicating which blocks are adjacent to each block (up, down, left and/or right)
	"""
	generator = BlockLayoutGenerator(structure_graph, start_node, left_weight, right_weight, up_weight, down_weight)
	layout, num_tries = retry(generator.generate, exception_type=LayoutError, max_tries=100)
	print("Obtained layout in {} attempts".format(num_tries))
	return layout

class BlockLayoutGenerator:
	def __init__(self, structure_graph, start_node, left_weight, right_weight, up_weight, down_weight):
		self.graph = structure_graph
		self.start_node = start_node
		self.left_weight = left_weight
		self.right_weight = right_weight
		self.up_weight = up_weight
		self.down_weight = down_weight
	
	def reset(self):
		"""Resets all of the generator's state variables to their initial values."""
		# A pair of dictionaries, mapping blocks to the nodes occupying them and vice-versa.
		self.blocks_to_nodes = {}
		self.nodes_to_blocks = {}
		self.visited_edges = set()
		self.adjacencies = defaultdict(set)
	
	def generate(self):
		self.reset()
		self.mark(block=(0, 0), node=self.start_node)
		while len(self.visited_edges) < self.graph.number_of_edges():
			self.position = (0, 0)
			self.node = self.start_node
			while True:
				# Follow an unvisited edge if possible, otherwise pick an already-visited one
				edges = self.graph.out_edges(self.node)
				if not edges:
					# We have found a dead end; the current path is complete.
					break
				unvisited_edges = list(filter(lambda edge: edge not in self.visited_edges, edges))
				edge = random.choice(unvisited_edges if unvisited_edges else edges)
				destination = edge[1]
				# If there were any unvisited edges, we have chosen one of them
				if unvisited_edges:
					self.visited_edges.add(edge)
					if destination in self.nodes_to_blocks:
						path = list(self.path(self.position, self.nodes_to_blocks[destination]))
						for block in itertools.islice(path, 1, len(path) - 1):
							self.mark(block, node=InterstitialNode())
						for a, b in pairs(path):
							self.add_adjacency(a, b)
						self.position = self.nodes_to_blocks[destination]
						self.node = destination
					else:
						# Map each unvisited node to a new unoccupied block.
						# Add an interstitial node between the source and destination, to
						# space out the interesting parts of the level.
						self.step(InterstitialNode())
						self.step(destination)
				else:
					# If we visited this edge already, just skip to its destination and don't
					# add anything.
					self.position = self.nodes_to_blocks[destination]
					self.node = destination
		return self.blocks_to_nodes, self.adjacencies
	
	def path(self, source, destination, bfs_iteration_limit=32):
		"""Finds the shortest path across the grid from the source block to the destination block, using only free blocks along the way.
		The path is returned as a sequence of blocks, from source to destination."""
		predecessors = self.bfs(source, destination, bfs_iteration_limit)
		path = [destination]
		# Follow the chain of predecessors to find the path, from destination to source, then reverse it
		cur = destination
		while cur != source:
			cur = predecessors[cur]
			path.append(cur)
		return reversed(path)
	
	def bfs(self, source, destination, iteration_limit=32, use_visited_blocks=False):
		"""Performs a breadth-first search from the source block across the free blocks on the grid, recording the immediate predecessor of each block. The search ends once the destination block is reached, or the depth of the search surpasses iteration_limit.
		
		If use_visited_blocks is set to True (it's False by default), occupied blocks
		will be considered in the search as well.
		
		Returns a dictionary containing the predecessors of each block processed."""
		predecessors = {}
		# The "front" is the set of blocks being examined in each iteration of the search
		front = {source}
		visited = {source}
		for i in range(iteration_limit):
			if not front:
				raise DestinationUnreachable("No possible path from {} to {} using only free blocks".format(source, destination))
			new_front = set()
			for block in front:
				# Expand the search to all free blocks adjacent to this block
				for target in all_directions(block):
					if target not in new_front and target not in visited and (target not in self.blocks_to_nodes or target == destination):
						new_front.add(target)
						visited.add(target)
						predecessors[target] = block
						if target == destination:
							return predecessors
			front = new_front
			visited.update(front)
		# If the outer loop ran to completion, we didn't find the destination
		raise DestinationNotReached("No path from {} to {} found; {} iterations may not be sufficient".format(source, destination, iteration_limit))
	
	def add_adjacency(self, a, b):
		self.adjacencies[a].add(b)
		self.adjacencies[b].add(a)
	
	def mark(self, block, node):
		"""Marks the given block as occupied by the corresponding node."""
		self.blocks_to_nodes[block] = node
		self.nodes_to_blocks[node] = block
	
	def step(self, node):
		"""Advances the generator's position in the grid by one block, moving in a randomly-chosen direction; then occupies that block with the given node."""
		# Choose the direction in which to create a new edge on the grid, using a weighted
		# random pick
		left, right, up, down = all_directions(self.position)
		choices = [(left, self.left_weight), (right, self.right_weight), (up, self.up_weight), (down, self.down_weight)]
		# Allow only the choices that lead to unoccupied blocks
		possible_choices = list(filter(lambda entry: entry[0] not in self.blocks_to_nodes, choices))
		if not possible_choices:
			raise OutOfSpace("no unoccupied blocks found to extend layout")
		direction = WeightedChoice(possible_choices).choice()
		# Mark the new blocks as adjacent
		self.add_adjacency(self.position, direction)
		# Advance to the new block and occupy it
		self.position = direction
		self.mark(block=direction, node=node)
		self.node = node

def bounding_box(block_map):
	"""Returns a tuple of the maximum and minimum X and Y coordinates of the specified
	block map, in the order min(X), max(X), min(Y) and max(Y)."""
	xs = tuple(map(lambda block: block[0], block_map.keys()))
	ys = tuple(map(lambda block: block[1], block_map.keys()))
	return min(xs), max(xs), min(ys), max(ys)

def visualization(block_map, adjacencies, block_width=24):
	from pygame import Surface, Color, draw
	xs = tuple(map(lambda block: block[0], block_map.keys()))
	ys = tuple(map(lambda block: block[1], block_map.keys()))
	rng = random.Random()
	min_x, max_x = min(xs), max(xs)
	min_y, max_y = min(ys), max(ys)
	# Returns the position of the specified block on the image.
	def block_image_pos(block):
		h = block_width // 2
		return ((block[0] - min_x) * block_width + h, (block[1] - min_y) * block_width + h)
	width = max_x - min_x + 1
	height = max_y - min_y + 1
	chart = Surface(size=(width * block_width, height * block_width))
	chart.fill((255, 255, 255))
	# Draw lines between adjacent blocks
	for block, neighbors in adjacencies.items():
		for neighbor in neighbors:
			draw.line(chart, (0, 0, 0), block_image_pos(block), block_image_pos(neighbor), 2)
	# Each type of node will be marked with a different color
	type_colors = {}
	for block, node in block_map.items():
		kind = type(node)
		if kind not in type_colors:
			color = Color(0, 0, 0, 0)
			color.hsva = (rng.randint(0, 359), 100, 100, 100)
			type_colors[kind] = color
		draw.circle(chart, type_colors[kind], block_image_pos(block), 6)
	print(type_colors)
	return chart