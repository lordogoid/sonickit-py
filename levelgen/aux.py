import itertools
import bisect
import random

class WeightedChoice:
	"""A class that makes it simpler to choose a random item from a list with weighted probabilities.
	
	This list is represented as a sequence of 2-tuples, each containing the option and its weight. The probability of each option being chosen is equal to its weight divided by the sum of all weights. An example of such a list follows:
	
	[("spam", 1), ("eggs", 5), ("snakes", 3)]
	"""
	def __init__(self, choice_table):
		self.choices = [choice for choice, weight in choice_table]
		self.cumulative_weights = list(itertools.accumulate(weight for choice, weight in choice_table))
	
	def choice(self):
		pick = random.uniform(0, self.cumulative_weights[-1])
		return self.choices[bisect.bisect(self.cumulative_weights, pick)]

def add(a, b):
	"""Adds two tuples of two numbers, element-wise."""
	return (a[0] + b[0], a[1] + b[1])

def all_directions(coordinates):
	"""Returns the result of moving one step left, right, up, and down from the given coordinates, in that order."""
	left = add(coordinates, (-1, 0))
	right = add(coordinates, (1, 0))
	up = add(coordinates, (0, -1))
	down = add(coordinates, (0, 1))
	return left, right, up, down