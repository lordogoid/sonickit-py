from sonickit.object import solid_sprite
from sonickit.animation import Animation
from sonickit.resources import set_default_pool, image, rel

set_default_pool("slz")

QuarterPipe = solid_sprite("QuarterPipe", __name__, rel("art/qpipe.png"))
OrangePlaf = solid_sprite("OrangePlaf", __name__, rel("art/orangeplaf.png"))
Ledge = solid_sprite("Ledge", __name__, rel("art/ledge.png"))
LedgeGrass = solid_sprite("LedgeGrass", __name__, rel("art/grass.png"))
Ledge.bottom_solid = False
Ledge.left_solid = False
Ledge.right_solid = False