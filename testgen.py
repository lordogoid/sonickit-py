import random
import argparse
import pickle
from sonickit import Runner, resources, level, builtins
from levelgen import structure, layout, implementation
from ghz import objects, style
from sonickit.players.sonic import Sonic
from pygame import image

runner = Runner()
resources.load_global()
"""
test_level = level.Level(spawn_positions={"default": (32, 0, 1)}, background=GHZBackground())
hmap = terrain.HeightMap(width=1024, starting_height=320, height_iterator=lambda height: random.gauss(height, 80),
                         flat_weight=3, slope_weight=6, ramp_weight=3, discontinuity_probability=0.3)
platform = GHZPlatform(x=hmap.width / 2, y=hmap.maximum_height / 2 + 100, layer=1, height_map=hmap)
test_level.add(platform)
sign = builtins.EndSign(x=900, y=platform.rect.bottom - hmap(900), layer=1)
sign.y -= sign.height / 2
test_level.add(sign)"""

parser = argparse.ArgumentParser()
parser.add_argument("random_mode", choices=("load", "save"))
parser.add_argument("state_file")
args = parser.parse_args()

if args.random_mode == "load":
	with open(args.state_file, "rb") as file:
		random.setstate(pickle.load(file))
elif args.random_mode == "save":
	with open(args.state_file, "wb") as file:
		pickle.dump(obj=random.getstate(), file=file)

graph, start = structure.generate_structure_graph(split_chance=0.15, merge_chance=0, dead_end_chance=0.1, max_branching_factor=2, max_parallel_paths=3, target_size=30, node_types={structure.BonusArea: 2, structure.ChallengeArea: 4})
structure.draw_graph_to_file(graph=graph, file="generated_graph.pdf")
blocks, adjacencies = layout.generate_block_layout(graph, start, left_weight=3, right_weight=100, up_weight=5, down_weight=3)
image.save(layout.visualization(blocks, adjacencies), "generated_layout.png")
level = implementation.generate_level(blocks, adjacencies, objects.implementation_table, block_width=256, block_height=256, background=style.GHZBackground(), background_music=style.background_music)



level.place_player(Sonic())
runner.current_section = level
runner.run()