from ..player import Player
from ..resources import image, set_default_pool, rel
from ..animation import gen_frame_sequence as frameseq

set_default_pool("global")

class Sonic(Player):
	standing_anim = image((rel("art/sonic/stand.png"),))
	death_anim = image((rel("art/sonic/death.png"),))
	walking_anim = image(frameseq(rel("art/sonic/walk/{}.png"), 8))
	running_anim = image(frameseq(rel("art/sonic/run/{}.png"), 4))
	spinning_anim = image(frameseq(rel("art/sonic/spin/{}.png"), 4))
	crouching_anim = image(frameseq(rel("art/sonic/crouch/{}.png"), 2))
	looking_up_anim = image(frameseq(rel("art/sonic/look_up/{}.png"), 2))
	springing_anim = image(rel("art/sonic/spring.png"))
	balancing_anim = image(frameseq(rel("art/sonic/balance/{}.png"), 3))
	braking_anim = image(frameseq(rel("art/sonic/brake/{}.png"), 3))
	damage_anim = image(frameseq(rel("art/sonic/damage/{}.png"), 2))
	end_level_anim = image([rel("art/sonic/end_level/{}.png".format(i)) for i in (1, 2, 3, 4, 3)])