from pygame.mixer import music
from pygame import USEREVENT

# These variables will be used to start playing the loop once the intro ends
current_loop = None
looping = False

music.set_endevent(USEREVENT)

def play(filename_intro, filename_loop):
	"""Begins playing the audio in the given files as background music, looping indefinitely.
	First, the intro track will be played, then the loop will be played, repeating forever.
	
	Any music already playing in this module will be replaced."""
	music.load(filename_intro)
	music.play()
	global current_loop, looping
	current_loop = filename_loop
	looping = False
	
pause = music.pause
unpause = music.unpause

def on_end_bgm():
	global looping
	if not looping:
		looping = True
		music.load(current_loop)
		music.play(loops=-1)