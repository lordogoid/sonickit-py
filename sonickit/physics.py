import math

class Quadrant:
	def __init__(self, **args):
		for key, value in args.items():
			setattr(self, key, value)

down_sensor = lambda p, o: o.vd(p.px, p.py, p.vl, 1)
up_sensor = lambda p, o: o.vd(p.px, p.py, p.vl, -1)
right_sensor = lambda p, o: o.hd(p.px, p.py, p.hl, 1)
left_sensor = lambda p, o: o.hd(p.px, p.py, p.hl, -1)

# The four physics quadrants (see the Sonic Physics Guide for details).
# The base values are actually the quantity that must be added to the measured angle to give the real angle.
# p = player
# o = object			
quadrants = dict(
	floor = Quadrant(
		name = "floor",
		base = 0,
		left = "right_wall",
		right = "left_wall",
		a = lambda p, o: o.vd(p.px - p.hw, p.py, p.vl, 1),
		b = lambda p, o: o.vd(p.px + p.hw, p.py, p.vl, 1),
		e = lambda p, o: o.hd(p.px, p.py - p.ho, p.hl, 1),
		f = lambda p, o: o.hd(p.px, p.py + p.ho, p.hl, 1),
		g = lambda p, o: o.hd(p.px, p.py - p.ho, p.hl, -1),
		h = lambda p, o: o.hd(p.px, p.py + p.ho, p.hl, -1),
		x = lambda p, o: o.vd(p.px, p.py, p.vl, 1)
	),
	left_wall = Quadrant(
		name = "left_wall",
		base = math.pi / 2,
		left = "floor",
		right = "ceiling",
		a = lambda p, o: o.hd(p.px, p.py - p.hw, p.vl, -1),
		b = lambda p, o: o.hd(p.px, p.py + p.hw, p.vl, -1),
		e = lambda p, o: o.vd(p.px + p.ho, p.py, p.hl, 1),
		f = lambda p, o: o.vd(p.px - p.ho, p.py, p.hl, 1),
		g = lambda p, o: o.vd(p.px + p.ho, p.py, p.hl, -1),
		h = lambda p, o: o.vd(p.px - p.ho, p.py, p.hl, -1)
	),
	ceiling = Quadrant(
		name = "ceiling",
		base = math.pi,
		left = "left_wall",
		right = "right_wall",
		a = lambda p, o: o.vd(p.px + p.hw, p.py, p.vl, -1),
		b = lambda p, o: o.vd(p.px - p.hw, p.py, p.vl, -1),
		e = lambda p, o: o.hd(p.px, p.py + p.ho, p.hl, -1),
		f = lambda p, o: o.hd(p.px, p.py - p.ho, p.hl, -1),
		g = lambda p, o: o.hd(p.px, p.py + p.ho, p.hl, 1),
		h = lambda p, o: o.hd(p.px, p.py - p.ho, p.hl, 1)
	),
	right_wall = Quadrant(
		name = "right_wall",
		base = -math.pi / 2,
		left = "ceiling",
		right = "floor",
		a = lambda p, o: o.hd(p.px, p.py + p.hw, p.vl, 1),
		b = lambda p, o: o.hd(p.px, p.py - p.hw, p.vl, 1),
		e = lambda p, o: o.vd(p.px - p.ho, p.py, p.hl, -1),
		f = lambda p, o: o.vd(p.px + p.ho, p.py, p.hl, -1),
		g = lambda p, o: o.vd(p.px - p.ho, p.py, p.hl, 1),
		h = lambda p, o: o.vd(p.px + p.ho, p.py, p.hl, 1)
	),
	air = Quadrant(
		name = "air",
		base = 0,
		a = lambda p, o: o.vd(p.px - p.hw, p.py, p.vl, 1),
		b = lambda p, o: o.vd(p.px + p.hw, p.py, p.vl, 1),
		c = lambda p, o: o.vd(p.px - p.hw, p.py, p.vl, -1),
		d = lambda p, o: o.vd(p.px + p.hw, p.py, p.vl, -1),
		e = lambda p, o: o.hd(p.px, p.py, p.hl, 1),
		g = lambda p, o: o.hd(p.px, p.py, p.hl, -1),
		# These sensors are used to enable landing on steep slopes
		i = lambda p, o: o.hd(p.px, p.py + p.hw, p.vl, 1),
		j = lambda p, o: o.hd(p.px, p.py - p.hw, p.vl, 1),
		k = lambda p, o: o.hd(p.px, p.py + p.hw, p.vl, -1),
		l = lambda p, o: o.hd(p.px, p.py - p.hw, p.vl, -1)
	)
)
