from ..object import Object

def find_classes(*modules):
	"""Finds all subclasses of sonickit.object.Object contained within the given modules. Does not recurse into nested modules.
	Returns a mapping from the classes' fully-qualified names to the classes themselves."""
	result = {}
	for module in modules:
		for name in dir(module):
			item = getattr(module, name)
			if isinstance(item, type) and issubclass(item, Object):
				result[item.__name__] = item
	return result