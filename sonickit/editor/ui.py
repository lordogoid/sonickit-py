from threading import Thread
from contextlib import contextmanager

import pygame
import pygame.locals as K
from pygame import display, event

from .. import resources, store, level, text, builtins
from .objlist import find_classes
				
class DummyPlayer:
	"""A dummy player used to prevent the level drawing code from crashing due to the lack of player object."""
	def update(self): pass
	def draw(self, *args): pass
	def set_pos(self, *args): pass
	
	rings = 0
	lives = 3
	mode = "ground"
	layer = 1
	score = 0
	
# Pygame's mouse button numbers
LEFT_BUTTON = 1
RIGHT_BUTTON = 3

def draw_lines(lines, font, surface, x, y, yspace = 12):
	"""Draws the given lines (provided as an iterable) of text near the upper-left corner of surface."""
	for line in lines:
		font.draw_text(line, surface, x, y)
		y += yspace
		
class ClassPicker:
	"""This class implements a simple mechanism for selecting classes and filtering the list."""
	def __init__(self, classes):
		self.classes = classes
		assert classes, "class list should not be empty"
		self.names = list(classes.keys())
		self.cur_list = self.names
		self.index = 0
	
	def next(self):
		self.index = (self.index + 1) % len(self.names)
	
	def prev(self):
		self.index = (self.index - 1) % len(self.names)
		
	@property
	def selected_name(self):
		return self.cur_list[self.index]
		
	@property
	def selected_class(self):
		return self.classes[self.selected_name]

class Editor:
	def __init__(self, fname, resolution = (800, 600)):
		self.main_window = display.set_mode(resolution)
		self.fname = fname
		self.level_config = store.load(fname)
		# This is set to true when the level loads
		self.ready = False
		def setready():
			self.ready = True
			# Force the main loop to continue
			event.post(event.Event(pygame.USEREVENT))
		resources.load_global()
		self.level = level.load(self.level_config, callback=setready)
		self.classes = find_classes(*(level.get_class(name) for name in self.level_config["object_path"]))
		self.level.place_player(DummyPlayer())
		self.font = text.fonts["classic"]
		self.selected_object = None
		self.selected_layer = 1
		c = self.level.camera
		c.width = resolution[0]
		c.height = resolution[1]
		self.picker = ClassPicker(self.classes)
		
	def true_pos(self, pos):
		"""Returns the playfield coordinates of a point relative to the camera's position."""
		camera = self.level.camera
		return pos[0] + camera.x, pos[1] + camera.y
		
	def place_object(self, pos, layer):
		if self.picker.selected_class is None:
			return
		self.level.add(self.picker.selected_class(pos[0], pos[1], layer))
		
	camera_motion_step = 100
	
	def key_left(self):
		self.level.camera.x -= self.camera_motion_step
	
	def key_right(self):
		self.level.camera.x += self.camera_motion_step
		
	def key_up(self):
		self.level.camera.y -= self.camera_motion_step
	
	def key_down(self):
		self.level.camera.y += self.camera_motion_step
	
	@contextmanager	
	def moving(self, obj):
		self.level.remove(obj)
		yield obj
		self.level.add(obj)
	
	def key_f(self):
		if self.selected_object is not None:
			with self.moving(self.selected_object) as obj:
				obj.layer += 1
		self.selected_layer += 1
	
	def key_b(self):
		if self.selected_layer > 0:
			if self.selected_object is not None:
				with self.moving(self.selected_object) as obj:
					obj.layer -= 1
			self.selected_layer -= 1
		
	def key_d(self):
		if self.selected_object is not None:
			self.level.remove(self.selected_object)
			self.selected_object = None
	
	def key_q(self):
		self.picker.prev()
	
	def key_w(self):
		self.picker.next()
	
	keytable = {
		K.K_LEFT: key_left,
		K.K_RIGHT: key_right,
		K.K_UP: key_up,
		K.K_DOWN: key_down,
		K.K_f: key_f,
		K.K_b: key_b,
		K.K_d: key_d,
		K.K_q: key_q,
		K.K_w: key_w
	}
		
	def keydown(self, ev):
		if ev.key in self.keytable:
			self.keytable[ev.key](self)
	
	def mousebuttondown(self, ev):
		pos = self.true_pos(ev.pos)
		if ev.button == RIGHT_BUTTON:
			self.place_object(pos, self.selected_layer)
		elif ev.button == LEFT_BUTTON:
			self.selected_object = self.level.object_at(pos[0], pos[1], self.selected_layer)
	
	def mousemotion(self, ev):
		selection = self.selected_object
		if selection is not None and LEFT_BUTTON in ev.buttons:
			with self.moving(selection):
				selection.x += ev.rel[0]
				selection.y += ev.rel[1]
			
	def mousebuttonup(self, ev):
		pass
		
	events = {
		pygame.KEYDOWN: keydown,
		pygame.MOUSEBUTTONDOWN: mousebuttondown,
		pygame.MOUSEMOTION: mousemotion,
		pygame.MOUSEBUTTONUP: mousebuttonup
	}
	
	def draw(self):
		camera = self.level.camera
		obj = self.selected_object
		self.level.draw(self.main_window, rem_time=1)
		lines = ["layer {}".format(self.selected_layer),
		         "camera pos {} {}".format(camera.x, camera.y),
				 "brush {}".format(self.picker.selected_name.lower())]
		if obj is not None:
			lines.append("selection {} at {} {}".format(type(obj).__name__.lower(), obj.x, obj.y))
		draw_lines(lines, font = self.font, surface = self.main_window, x = 4, y = 4)
		
	def mainloop(self):
		resources.load_global()
		while 1:
			self.main_window.fill((0x80, 0x80, 0x80))
			if self.ready:
				self.draw()
			display.flip()
			ev = event.wait()
			if ev.type == pygame.QUIT:
				self.level_config["objects"] = self.level.dump_objects()
				store.dump(self.fname, self.level_config)
				return
			elif ev.type in self.events:
				self.events[ev.type](self, ev)