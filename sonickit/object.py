import math

from pygame import Rect, Surface
from . import resources, store, logging
from .resources import sound, get_sound, rel
from .animation import Animation, StaticFrame, Blank, gen_frame_sequence as frameseq
from .physics import quadrants

inf = float("inf")

__all__ = ["Object", "SpriteObject", "BlockSpriteObject", "PhysicsObject",
           "GroundFollowingObject", "MovingObject", "DestructibleObject", "Enemy", "Bullet", "solid_sprite"]

class Object:
	"""This class represents any game object with a bounding box."""
	def __init__(self, x, y, layer):
		self.x = x
		self.y = y
		self.layer = layer
	
	# By default, objects don't move. The attributes are here so that code that cares about
	# the speed of an object doesn't need to check if they are there or not	
	x_speed = 0
	y_speed = 0
	
	ignore_physics = True
		
	@property
	def rect(self):
		return Rect(self.x - self.width / 2, self.y - self.height / 2, self.width, self.height)
	
	# Stub implementations for the collision callbacks
	def on_collision(self, obj): pass
	def on_floor_collision(self, obj): pass
	def on_ceiling_collision(self, obj): pass
	def on_left_wall_collision(self, obj): pass
	def on_right_wall_collision(self, obj): pass
	
	def draw(self, surface, rect, rem_time): pass
	def update(self): pass
	
	def reset(self):
		"""Restores this object to its initial state. Subclasses that have mutable state should
		override this to restore their state."""
		pass
	
	def dump(self):
		"""Returns a tuple which can later be used by level.load to reconstruct this object.
		The tuple contains the name of the object's class and the arguments to pass to its constructor."""
		return (store.fullname(type(self)), self.x, self.y, self.layer)
	
"""The alpha value above which a pixel is considered solid."""
ALPHA_THRESHOLD = 192
	
class SpriteObject(Object):
	"""This class represents an object that has a visible sprite and a collision mask.
	The object must have an animation property containing an Animation or StaticFrame object.
	
	The object's size is determined automatically by the size of the first frame of that animation,
	which must be set before this class's constructor is invoked."""
	def __init__(self, x, y, layer):
		super().__init__(x, y, layer)
		self.set_size()
		
	def set_size(self):
		"""Sets the size of this object based on the size of the current frame of its animation."""
		image = self.animation.current_image
		self.width = image.get_width()
		self.height = image.get_height()
		
	def update(self):
		self.animation.update()
		
	ignore_physics = False
	top_solid = True
	bottom_solid = True
	left_solid = True
	right_solid = True
	
	@property
	def collision_mask(self):
		"""The surface containing this object's collision mask.
		The solidity of each pixel is determined by its alpha value."""
		return resources.get_image(self.collision_mask_name)
	
	def pixel_distance(self, x, y, n, dx, dy):
		"""Returns the distance from the point (x, y) to the collision map's surface
		along a line determined by the vector (dx, dy) at most n pixels long.
		If the line does not intersect the surface, returns infinity."""
		rect = self.rect
		x -= rect.left
		y -= rect.top
		mask = self.collision_mask
		for i in range(n):
			if 0 <= x < self.width and 0 <= y < self.height and mask.get_at((x, y)).a > 192:
				return i
			x += dx
			y += dy
		return float("inf")
		
	def vertical_distance(self, x, y, n, dy):
		return self.pixel_distance(x, y, n, 0, dy)
	
	def horizontal_distance(self, x, y, n, dx):
		return self.pixel_distance(x, y, n, dx, 0)
		
	vd = vertical_distance
	hd = horizontal_distance
		
	def draw(self, surface, camera, rem_time):
		rect = self.rect
		self.animation.draw(surface, (rect.left - camera.x, rect.top - camera.y))
		
class BlockSpriteObject(SpriteObject):
	"""This is a special kind of SpriteObject whose collision mask is always a solid rectangle
	the same size as its sprite."""
	
	def __init__(self, x, y, layer):
		super().__init__(x, y, layer)
		self.collision_surface = Surface((self.width, self.height))
	
	@property
	def collision_mask(self):
		return self.collision_surface
		
class PhysicsObject(Object):
	"""This class is intended as a mixin for objects that need to detect collisions with
	other objects.
	
	"""
	hl = 16
	vl = 40
	
	@property
	def px(self):
		return int(self.x)
	
	@property
	def py(self):
		return int(self.y)
	
	def should_hit_floor(self, dist):
		return self.y_speed > 0 and self.height / 2 > dist
	
	def should_hit_ceiling(self, dist):
		return self.y_speed < 0 and self.height / 2 > dist
	
	def should_hit_left_wall(self, dist):
		return self.x_speed < 0 and self.width / 2 > dist
	
	def should_hit_right_wall(self, dist):
		return self.x_speed > 0 and self.width / 2 > dist
	
	# TODO: This is the same as Player.collision_rect. This should be deduplicated eventually
	@property
	def collision_rect(self):
		return Rect(self.x - (self.width + 2) / 2, self.y - (self.height + 2) / 2, self.width + 2, self.height + 2)

class GroundFollowingObject(PhysicsObject):
	"""A special kind of PhysicsObject that receives information on the angle of the ground
	like a player, via collide_ground. Only the two downwards (floor.a and floor.b) ground sensors
	are provided."""
	def __init__(self, x, y, layer):
		super().__init__(x, y, layer)
		self.angle = 0
	
	@property
	def hw(self):
		return int(self.width / 2 - 1)
		
	def collide_walls(self, lh, rh):
		with self.level.move(self):
			if lh != inf:
				self.x += lh - self.width / 2
			elif rh != inf:
				self.x -= rh - self.width / 2
	
	def collide_ground(self, ah, bh, lh, rh):
		if ah != inf and bh != inf:
			self.angle = math.atan2(bh - ah, 2 * self.hw)
		else:
			self.on_hit_ledge()
		dist = min(ah, bh)
		if dist != inf:
			with self.level.move(self):
				self.y -= self.height / 2 - dist
	
	def on_hit_ledge(self):
		"""Called when this object is near the edge of the platform it is on. By default,
		does nothing."""
		pass

class MovingObject(Object):
	"""A superclass for objects that can move.
	This takes care of properly resetting the object's position when needed."""
	def __init__(self, x, y, layer):
		self.initial_x = x
		self.initial_y = y
		super().__init__(x, y, layer)
	
	def set_initial_position(self):
		"""Sets this object's initial position to its current position.
		Useful if you set this object's position externally (for example, with
		place_object_atop) after creating it."""
		self.initial_x = self.x
		self.initial_y = self.y
	
	def reset(self):
		with self.level.move(self):
			self.x = self.initial_x
			self.y = self.initial_y
		super().reset()

class DestructibleObject(BlockSpriteObject):
	"""This is the superclass for all objects that can be destroyed and release a puff of smoke
	when that happens, such as item monitors, enemies, and bombs.
	
	Subclasses of this must define an initial_animation and destroyed_animation attributes."""
	smoke_frames = resources.image(frameseq(resources.rel("art/poof/{}.png"), 4), "global")
	
	explosion_sound = sound(rel("sfx/objects/explosion.wav"), "global")
	
	def __init__(self, x, y, layer):
		super().__init__(x, y, layer)
		self.smoke_animation = Animation(self.smoke_frames, interval=10, vanishing=True)
	
	def destroy(self):
		"""Destroys this object and begins playing the smoke animation."""
		self.animation = self.destroyed_animation
		self.ignore_physics = True
		get_sound(self.explosion_sound).play()
		
	@property
	def destroyed(self):
		"""Returns True if this object has been destroyed."""
		return self.animation is self.destroyed_animation
	
	def draw(self, surface, camera, rem_time):
		super().draw(surface, camera, rem_time)
		# If this object has been destroyed, draw a puff of smoke over the destroyed sprite
		if self.destroyed:
			rect = self.rect
			# Draw smoke over the center of the rect
			cx = rect.x - camera.x + rect.width / 2
			cy = rect.y - camera.y + rect.height / 2
			# TODO: Replace 16's with half the actual dimensions of smoke sprite
			# Which, ironically, turns out to be 16 after all.
			self.smoke_animation.draw(surface, (cx - 16, cy - 16))
	
	def update(self):
		super().update()
		if self.destroyed:
			self.smoke_animation.update()
	
	def reset(self):
		super().reset()
		self.animation = self.initial_animation
		self.initial_animation.reset()
		self.destroyed_animation.reset()
		self.smoke_animation.reset()
		self.ignore_physics = False

class Enemy(DestructibleObject):
	"""This is the superclass for all enemies. When a player makes contact with an enemy,
	the latter is destroyed if the player is spinning; otherwise, the player takes damage."""
	
	def __init__(self, x, y, layer):
		self.destroyed_animation = Blank()
		super().__init__(x, y, layer)
	
	def on_floor_collision(self, player):
		if not hasattr(player, "jump"):
			return
		if player.spinning and not self.destroyed:
			self.destroy()
			player.rebound_enemy(self.y)
		else:
			if player.damage(self.x):
				logging.player_took_damage_from(type(self))
	
	def destroy(self):
		logging.player_killed_enemy(type(self))
		super().destroy()
	
	on_ceiling_collision = on_floor_collision
	on_left_wall_collision = on_floor_collision
	on_right_wall_collision = on_floor_collision
	
class Bullet(BlockSpriteObject):
	"""This class implements a simple bullet that moves at a set speed and damages the player
	upon contact. After that, it disappears."""
	ignore_physics = True
	animation = StaticFrame(resources.image(resources.rel("art/bullet.png"), "global"))
	
	def __init__(self, x, y, layer, x_speed, y_speed):
		self.x_speed = x_speed
		self.y_speed = y_speed
		super().__init__(x, y, layer)
	
	def update(self):
		with self.level.move(self):
			self.x += self.x_speed
			self.y += self.y_speed
		super().update()
	
	def on_collision(self, player):
		player.damage(self.x)
		self.level.remove(self)
		
def solid_sprite(name, module_name, sprite, pool=None):
	"""Returns a subclass of SpriteObject that draws the static sprite at the file sprite_name.
	The object is solid by default.
	Useful mainly for static platforms."""
	resources.image(sprite, pool)
	return type(name, (SpriteObject,), {
		"animation": StaticFrame(sprite),
		"collision_mask_name": sprite,
		"__module__": module_name
	})

class Trigger(Object):
	def __init__(self, x, y, layer, width, height):
		super().__init__(x, y, layer)
		self.width = width
		self.height = height