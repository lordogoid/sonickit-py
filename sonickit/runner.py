import pygame
from pygame import event, display, KEYDOWN, KEYUP, QUIT, USEREVENT
from . import keyboard, music

"""The amount of time, in milliseconds, that is simulated in each iteration of the engine."""
PHYSICS_TIME_SLICE = 1000 / 60

class Runner:
	def __init__(self, resolution = (426, 240)):
		self.keyboard = None
		self._current_section = None
		pygame.init()
		self.screen = display.set_mode(resolution)
		
	@property
	def current_section(self):
		return self._current_section
	
	@current_section.setter
	def current_section(self, section):
		"""Sets the currently-running screen to section and calls its start method."""
		self._current_section = section
		section.runner = self
		section.start()
	
	def run(self, fps = 60):
		clock = pygame.time.Clock()
		# Time that remains to be simulated.
		remtime = 0
		while 1:
			remtime += clock.tick(fps)
			for e in event.get():
				if e.type == QUIT:
					return
				elif e.type == KEYDOWN:
					keyboard.keydown(e.key)
				elif e.type == KEYUP:
					keyboard.keyup(e.key)
				elif e.type == USEREVENT:
					music.on_end_bgm()
			n, remtime = divmod(remtime, PHYSICS_TIME_SLICE)
			for _ in range(int(n)):
				keyboard.update()
				self.current_section.update()
				keyboard.del_tapped_keys()
			self.screen.fill((200, 200, 200))
			self.current_section.draw(self.screen, remtime / PHYSICS_TIME_SLICE)
			display.flip()
			