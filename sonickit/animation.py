"""The animation module provides a class for tracking the progress and speed of an animation,
as well as a helper function to generate frame sequences."""

from . import resources
from pygame.transform import flip

def gen_frame_sequence(pattern, num_frames):
	"""Creates a frame sequence for use in an animation. The pattern should
	be of the form path/to/{}.ext, where {} will be replaced by a number from 1 to numFrames."""
	return [pattern.format(i + 1) for i in range(num_frames)]
	

	
class Flippable:
	flip_x = flip_y = False
	
	@property
	def flipped_image(self):
		image = self.current_image
		if self.flip_x or self.flip_y:
			image = flip(image, self.flip_x, self.flip_y)
		return image
	
class StaticFrame(Flippable):
	"""This class allows a static frame to be used in place of an Animation instance, implementing the
	   same interface."""
	
	def __init__(self, frame):
		self.frame = frame
	
	@property
	def current_image(self):
		return resources.get_image(self.frame)
	
	def update(self): pass
	
	def draw(self, surface, rect):
		surface.blit(self.flipped_image, rect)
	
	def reset(self): pass

class Blank:
	"""This class implements a blank animation (that draws nothing on the screen.)"""
	@property
	def current_image(self):
		return None
	
	def update(self): pass
	def draw(self, surface, rect): pass
	def reset(self): pass

class Animation(Flippable):
	"""This class implements an animation, whose frames are specified by a filename list and whose speed is variable.
	
	Animations can be drawn flipped across the X and Y axes by setting the flip_x and flip_y
	attributes to true."""
	
	def __init__(self, frames, interval, restart_index = 0, vanishing=False):
		"""Creates a new animation with the given frame sequence.
		interval is the number of frames that the animation waits before advancing (this can be changed later by assigning to the interval attribute).
		restart_index is the index of the frame to which the animation returns in the end; it defaults to 0, which creates a looping animation. If None is given, the animation will stop at the end.
		If vanishing is True, then the animation will stop drawing once it reaches the end. This implicitly disables looping.
		By default, vanishing is False."""
		if restart_index is None or vanishing:
			restart_index = len(frames) - 1
		# Go through the inicial phase of the animation, then cycle through the rest
		self.frames = frames
		self.interval = interval
		self.current_frame = 0
		self.restart_index = restart_index
		self.time_until_next_frame = interval
		self.vanishing = vanishing
		self.vanished = False
		
	@property
	def current_image(self):
		return resources.get_image(self.frames[self.current_frame])
	
	def update(self):
		"""Updates the animation's cycle, switching frames if appropriate."""
		self.time_until_next_frame -= 1
		if self.time_until_next_frame == 0:
			self.time_until_next_frame = self.interval
			if self.current_frame == len(self.frames) - 1:
				self.current_frame = self.restart_index
			else:
				self.current_frame += 1
	
	def draw(self, surface, rect):
		"""Draws the current sprite of this animation at the location given by rect, unless
		it was created with vanishing=True and already showed its last frame."""
		if self.vanished:
			return
		surface.blit(self.flipped_image, rect)
		if self.vanishing and self.current_frame == len(self.frames) - 1:
			self.vanished = True
		
	def reset(self):
		"""Restarts the animation from the beginning."""
		self.current_frame = 0
		self.time_until_next_frame = self.interval
		self.vanished = False