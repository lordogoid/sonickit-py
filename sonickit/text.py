"""The text module provides a facility for writing text using bitmapped sprite fonts."""

import os
import glob
import string
from .resources import image, rel, get_image

"""A dictionary containing special names for some characters that may be inconvenient to use in filenames."""
charnames = {
	":": "colon",
	"'": "apos",
	'"': "quote"
}

class MissingCharError(Exception): pass

class Font:
	def __init__(self, directory, chars = string.ascii_lowercase + string.digits, space_width = 10):
		"""Initializes a font object from a directory of sprites whose names correspond to
		the characters they represent. These sprites should be named the same as the characters,
		except for those listed in text.charnames.
		chars should be an iterable of characters to be loaded; it defaults to all lowercase letters
		plus all digits."""
		self.char_sprites = {}
		self.space_width = space_width
		for char in chars:
			# If the character does not have a special name defined for it, its filename is itself
			name = charnames.get(char, char)
			# Allow any extension for the character data
			try:
				fname = glob.glob(os.path.join(directory, "{}.*".format(name)))[0]
			except IndexError as err:
				raise MissingCharError("character {} does not have a sprite in {}".format(char, directory)) from err
			self.char_sprites[name] = image(fname, "global")
	
	def draw_text(self, text, target, x, y):
		"""Draws text using this font onto the target surface, placing the top-left corner at (x, y).
		Any characters not in the font's character set will be drawn as spaces."""
		for char in str(text):
			if char == " " or char not in self.char_sprites:
				x += self.space_width
				continue
			img = get_image(self.char_sprites[char])
			target.blit(img, (x, y))
			x += img.get_width()
			
fonts = {"classic": Font(rel("art/fonts/classic"))}