import math

from sonickit.object import *
from sonickit.animation import StaticFrame, Animation, Blank, gen_frame_sequence as frameseq
from sonickit.resources import rel, image, set_default_pool, sound, get_sound
from sonickit import logging
from pygame import Surface

set_default_pool("global")

__all__ = ["Spikes", "Ring"]

class Spikes(BlockSpriteObject):
	spike_image = image(rel("builtins_art/spikes.png"), "global")
	animation = StaticFrame(spike_image)
	
	hit_sound = sound(rel("sfx/objects/spikes.wav"), "global")
	
	def on_floor_collision(self, player):
		player.damage(self.x)
		logging.player_took_damage_from(type(self))
		get_sound(self.hit_sound).play()

class Ring(BlockSpriteObject):
	spinning_frames = image(frameseq(rel("builtins_art/ring/spin/{}.png"), 4), "global")
	sparkle_frames = image(frameseq(rel("builtins_art/ring/sparkle/{}.png"), 4), "global")
	
	ring_sound = sound(rel("sfx/objects/ring_collect.wav"), "global")
	
	ignore_physics = True
	
	def __init__(self, x, y, layer):
		self.spinning_animation = Animation(self.spinning_frames, interval=8)
		self.sparkle_animation = Animation(self.sparkle_frames, interval=8, vanishing=True)
		self.animation = self.spinning_animation
		super().__init__(x, y, layer)
	
	def on_collision(self, player):
		if self.animation is self.spinning_animation and player.can_get_rings:
			get_sound(self.ring_sound).stop()
			get_sound(self.ring_sound).play()
			player.rings += 1
			self.animation = self.sparkle_animation
	
	def reset(self):
		self.spinning_animation.reset()
		self.sparkle_animation.reset()
		self.animation = self.spinning_animation

class BouncingRing(Ring, PhysicsObject, MovingObject):
	def __init__(self, x, y, layer, x_speed, y_speed):
		self.x_speed = x_speed
		self.y_speed = y_speed
		self.remaining_duration = self.duration
		super().__init__(x, y, layer)
		
	duration = 256
		
	gravity = 0.09375
	
	def on_collision(self, player):
		if self.animation is self.spinning_animation and player.can_get_rings:
			logging.player_collected_bouncing_ring()
		super().on_collision(player)
	
	def update(self):
		self.remaining_duration -= 1
		if self.remaining_duration == 0:
			self.level.remove(self)
			return
		self.y_speed += self.gravity
		with self.level.move(self):
			self.x += self.x_speed
			self.y += self.y_speed
		super().update()
	
	def on_floor_collision(self, object):
		self.y_speed *= -.75
	
	on_ceiling_collision = on_floor_collision
	
	def on_left_wall_collision(self, object):
		self.x_speed *= -.25
	
	def reset(self):
		# Bouncing rings are supposed to vanish forever if the player dies
		pass
		
	on_right_wall_collision = on_left_wall_collision
	
class Monitor(DestructibleObject):
	"""A monitor that does nothing when destroyed."""
	sprite_name = image(rel("builtins_art/monitor.png"), "global")
	destroyed_sprite_name = image(rel("builtins_art/monitor_destroyed.png"), "global")
	
	def __init__(self, x, y, layer):
		self.initial_animation = StaticFrame(self.sprite_name)
		self.destroyed_animation = StaticFrame(self.destroyed_sprite_name)
		self.animation = self.initial_animation
		super().__init__(x, y, layer)
	
	def on_floor_collision(self, player):
		if player.spinning and not self.destroyed:
			self.apply_bonus(player)
			logging.player_hit_monitor(type(self))
			self.destroy()
			player.rebound_monitor(self.y)
			
	def apply_bonus(self, player):
		"""Applies this monitor's bonus effect to the player. By default, this does nothing;
		subclasses should implement their effect here."""
		pass
	
	on_left_wall_collision = on_ceiling_collision = on_right_wall_collision = on_floor_collision

class RingMonitor(Monitor):
	"""A monitor that grants a certain amount of rings (10 by default) to the player."""
	sprite_name = image(rel("builtins_art/monitor_ring.png"), "global")
	
	def __init__(self, x, y, layer, rings=10):
		self.rings = rings
		super().__init__(x, y, layer)
	
	def apply_bonus(self, player):
		player.rings += self.rings
		get_sound(Ring.ring_sound).play()
	
	def dump(self):
		return super().dump() + (self.rings,)

class LifeMonitor(Monitor):
	"""A monitor that grants one extra life to the player."""
	sprite_name = image(rel("builtins_art/monitor_1up.png"), "global")
	
	life_sound = sound(rel("sfx/objects/extra_life.wav"), "global")
	
	def apply_bonus(self, player):
		player.lives += 1
		get_sound(self.life_sound).play()

class ShieldMonitor(Monitor):
	"""A monitor that grants a temporary shield to the player. This shield will negate the
	next instance of damage, but not protect from effects that instantly kill it."""
	sprite_name = image(rel("builtins_art/monitor_shield.png"), "global")
	
	shield_sound = sound(rel("sfx/objects/shield.wav"), "global")
	
	def apply_bonus(self, player):
		player.shielded = True
		get_sound(self.shield_sound).play()

class YellowSpring:
	sprite = image(rel("builtins_art/spring/yellow_contracted.png"), "global")
	launch_speed = 10
	
	launch_sound = sound(rel("sfx/objects/spring.wav"), "global")

class HorizontalSpring(BlockSpriteObject):
	def launch(self, player):
		player.facing_left = self.facing_left
		player.control_lock_time = 16
		launch_speed = -self.launch_speed if self.facing_left else self.launch_speed
		if player.airborne:
			player.x_speed = launch_speed
		else:
			player.speed = launch_speed
		get_sound(self.launch_sound).play()

class UpYellowSpring(BlockSpriteObject, YellowSpring):
	animation = StaticFrame(image(rel("builtins_art/spring/yellow_vertical_contracted.png"), "global"))
	
	def on_floor_collision(self, player):
		if player.spinning:
			player.unroll()
		player.y_speed = -self.launch_speed
		player.go_airborne()
		player.animation = player.animations["springing"]
		get_sound(self.launch_sound).play()

class LeftYellowSpring(HorizontalSpring, YellowSpring):
	facing_left = True
	animation = StaticFrame(YellowSpring.sprite)
	animation.flip_x = True
	
	def on_left_wall_collision(self, player):
		self.launch(player)

class RightYellowSpring(HorizontalSpring, YellowSpring):
	facing_left = False
	animation = StaticFrame(YellowSpring.sprite)
	
	def on_right_wall_collision(self, player):
		self.launch(player)
		
class Motobug(Enemy, GroundFollowingObject, MovingObject):
	"""An enemy that rolls back and forth across the ground."""
	frames = image(frameseq(rel("builtins_art/motobug/{}.png"), 4), "global")
	
	speed = -2.5
	
	def __init__(self, x, y, layer):
		self.x_speed = self.y_speed = 0
		self.animation = self.initial_animation = Animation(self.frames, interval=15)
		super().__init__(x, y, layer)
		
	def update(self):
		if not self.destroyed:
			self.animation.flip_x = self.speed > 0
			with self.level.move(self):
				self.x_speed = self.speed * math.cos(self.angle)
				self.y_speed = self.speed * math.sin(self.angle)
				self.x += self.x_speed
				self.y += self.y_speed
		super().update()
	
	# Reverse direction when hitting walls
	def on_left_wall_collision(self, obj):
		if hasattr(obj, "jump"):
			super().on_left_wall_collision(obj)
		else:
			self.speed = -self.speed
			
	def on_hit_ledge(self):
		self.speed = -self.speed
	
	on_right_wall_collision = on_left_wall_collision

class BuzzBomber(Enemy, MovingObject):
	"""An enemy that flies in one direction across the air, shooting a bullet at the player
	when at a 45° angle to them."""
	initial_animation = StaticFrame(image(rel("builtins_art/buzzbomber/flying.png"), "global"))
	shooting_animation = StaticFrame(image(rel("builtins_art/buzzbomber/shooting.png"), "global"))
	
	speed = -3.5
	bullet_speed = 1.5
	bullet_offset = (-13, -19)
	
	# The time (in frames) the bomber spends in the firing stance
	shooting_time = 45
	
	x_proximity_threshold = 500
	y_proximity_threshold = 200
	# The bomber cannot fire more than once every X frames
	shooting_interval = 90
	
	def __init__(self, x, y, layer):
		self.x_speed = self.speed
		self.animation = self.initial_animation
		self.active = False
		self.rem_shooting_time = 0
		self.rem_shooting_interval = 0
		super().__init__(x, y, layer)
	
	def update(self):
		player = self.level.player
		# Trigger only if this is nearby and above and to the right of the player
		if 0 < self.x - player.x < self.x_proximity_threshold and 0 < player.y - self.y < self.y_proximity_threshold:
			self.active = True
		if not self.destroyed and self.active:
			self.rem_shooting_time -= 1
			self.rem_shooting_interval -= 1
			if self.rem_shooting_time == 0:
				self.animation = self.initial_animation
				self.x_speed = self.speed
			xdiff = player.x - self.x
			ydiff = player.y - self.y
			# Fire bullet if the bomber is above and to the right of the player's head
			# at an approximate 45° angle
			if xdiff < 0 and ydiff > 0 and -4 < abs(xdiff) - abs(ydiff) < 4 and self.rem_shooting_time <= 0 and self.rem_shooting_interval <= 0:
				self.animation = self.shooting_animation
				self.rem_shooting_time = self.shooting_time
				self.rem_shooting_interval = self.shooting_interval
				self.x_speed = 0
				self.level.add(Bullet(x=self.x - self.bullet_offset[0], y=self.y - self.bullet_offset[1],
				layer=self.layer, x_speed=-self.bullet_speed, y_speed=self.bullet_speed))
			with self.level.move(self):
				self.x += self.x_speed
		super().update()
	
	def reset(self):
		self.active = False
		self.rem_shooting_time = 0
		super().reset()

class Chopper(Enemy, MovingObject):
	"""An enemy that starts jumping up and down when the player gets too close."""
	animation = initial_animation = StaticFrame(image(rel("builtins_art/chopper/closed.png"), "global"))
	open_animation = StaticFrame(image(rel("builtins_art/chopper/open.png"), "global"))
	
	jumping = False
	"""When the Chopper's absolute Y speed is lower than this, open its mouth"""
	mouth_opening_threshold = 2.5
	jump_speed = 6.5
	gravity = 0.21875
	
	"""When the player is at least this many pixels close to the Chopper on the X axis,
	leap up."""
	proximity_threshold = 32
	
	def update(self):
		if not self.destroyed:
			if abs(self.level.player.x - self.x) < self.proximity_threshold and not self.jumping:
				self.y_speed = -self.jump_speed
				self.jumping = True
			if self.jumping:
				with self.level.move(self):
					self.y += self.y_speed
					self.y_speed += self.gravity
					# Bounce back up if we reach the approximate point where we started
					if self.y_speed > self.jump_speed:
						self.y = self.initial_y
						self.y_speed = -self.jump_speed
					if abs(self.y_speed) < self.mouth_opening_threshold:
						self.animation = self.open_animation
					else:
						self.animation = self.initial_animation
					self.animation.flip_y = self.y_speed > 0
		super().update()
	
	def reset(self):
		self.jumping = False
		super().reset()

class MovingPlatform(SpriteObject, MovingObject):
	"""A platform that moves back and forth between two locations at regular intervals."""
	basic_sprite = image(rel("builtins_art/generic_moving_platform.png"), "global")
	animation = StaticFrame(basic_sprite)
	collision_mask_name = basic_sprite
	
	bottom_solid = False
	left_solid = False
	right_solid = False
	
	def __init__(self, x, y, layer, end_x=0, end_y=0, period=240):
		"""Creates a moving platform.
		
		x and y are the initial position of the platform;
		end_x and end_y are the position to which it travels;
		period is the time (in frames) the platform takes to return to its starting point."""
		self.end_x = end_x
		self.end_y = end_y
		self.period = period
		self.counter = 0
		super().__init__(x, y, layer)
	
	def on_floor_collision(self, player):
		if player.attached_platform is not self:
			logging.player_hit_moving_platform()
		player.attach_to_platform(self)
	
	def update(self):
		# Use a sinusoid to determine the current position, so that the platform's movement
		# slows down smoothly near the endpoints and speeds up in the middle
		k = 2 * math.pi / self.period
		current_angle = self.counter * k
		# When c = 1, the platform is at the initial position; when c = 0, at the end one.
		c = (math.cos(current_angle) + 1) / 2
		old_x = self.x
		old_y = self.y
		with self.level.move(self):
			self.x = c * self.initial_x + (1 - c) * self.end_x
			self.y = c * self.initial_y + (1 - c) * self.end_y
		# Compute the X and Y speeds manually (for the sake of code which might need to query them) by taking the distance the platform moved in this frame
		self.x_speed = self.x - old_x
		self.y_speed = self.y - old_y
		self.counter += 1
		super().update()
	
	def dump(self):
		return super().dump() + (self.end_x, self.end_y, self.period)

class EndSign(BlockSpriteObject):
	ignore_physics = True
	frames = image([rel("builtins_art/end_sign/{}.png".format(i)) for i in (1, 2, 3, 4, 5, 2, 3, 4, 6)], "global")
	
	sign_sound = sound(rel("sfx/objects/end_sign.wav"), "global")
	
	def __init__(self, x, y, layer):
		self.animation = StaticFrame(self.frames[0])
		self.spinning_animation = Animation(self.frames, interval=6, restart_index=None)
		super().__init__(x, y, layer)
	
	def on_collision(self, player):
		self.animation = self.spinning_animation
		self.level.end()
		#get_sound(self.sign_sound).play()
		
LoopFront = solid_sprite("LoopFront", __name__, rel("builtins_art/loop/layer1.png"))
LoopBack = solid_sprite("LoopBack", __name__, rel("builtins_art/loop/layer0.png"))

class PathSwapper(Object):
	ignore_physics = True
	
	def __init__(self, x, y, layer, width=16, height=60, layer_forward=0, layer_backward=1):
		self.width = width
		self.height = height
		self.layer_forward = layer_forward
		self.layer_backward = layer_backward
		super().__init__(x, y, layer)
	
	def on_collision(self, player):
		if player.speed > 0:
			player.layer = self.layer_forward
		elif player.speed < 0:
			player.layer = self.layer_backward
	
#	def draw(self, surface, camera, rem_time):
#		surface.fill((255, 0, 0), (self.x - self.width / 2 - camera.x, self.y - self.height / 2 - camera.y, self.width, self.height))
	
	def dump(self):
		return super().dump() + (self.width, self.height, self.layer_forward, self.layer_backward)