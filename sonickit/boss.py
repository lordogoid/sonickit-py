import math
import random
from pygame import transform
from .object import BlockSpriteObject, MovingObject
from .animation import Animation, StaticFrame, gen_frame_sequence as frameseq
from .resources import rel, image, set_default_pool, get_image, sound, get_sound

class Boss(BlockSpriteObject, MovingObject):
	normal_animation = StaticFrame(image(rel("boss_art/eggpod.png"), "global"))
	hurt_animation = StaticFrame(image(rel("boss_art/eggpod_hit.png"), "global"))
	
	explosion_sprites = image(frameseq(rel("boss_art/explosion/{}.png"), 5), "global")
	explosion_frequency = 25
	
	"""Time in frames during which the boss is invulnerable after being struck."""
	hit_interval = 45
	"""The speed at which the boss flees after being defeated."""
	escape_speed = 3.5
	
	hit_flash_time = 20
	
	hit_sound = sound(rel("sfx/objects/boss_hit.wav"), "global")
	explosion_sound = sound(rel("sfx/objects/explosion_b.wav"), "global")
	
	def __init__(self, x, y, layer, hit_points=8):
		self.animation = self.normal_animation
		self.initial_hit_points = hit_points
		self.hit_points = hit_points
		self.flash_counter = 0
		self.invulnerability_time = 0
		self.flipped = True
		self.weapons = []
		self.movement_patterns = []
		super().__init__(x, y, layer)
	
	def reset(self):
		super().reset()
		self.hit_points = self.initial_hit_points
		
	def attach(self, weapon):
		"""Attaches a boss weapon to this boss. Boss weapons are separate objects that
		damage the player when touched and move along with the boss.
		This add the weapon into the level and sets a boss attribute on it, so that it can set
		its position relative to the boss's."""
		self.weapons.append(weapon)
		self.level.add(weapon)
	
	def add_movement_pattern(self, pattern):
		"""Adds a movement pattern to this boss. This can be any callable object that accepts
		the boss as an argument and modifies its X and Y speeds accordingly; this module
		provides some implementations of such objects."""
		self.movement_patterns.append(pattern)
	
	def on_floor_collision(self, player):
		# If the player is spinning, the boss takes damage; otherwise the player does, same as
		# with an enemy
		if player.spinning and self.hit_points > 0:
			was_flashing = self.flash_counter <= 0
			if self.invulnerability_time <= 0:
				get_sound(self.hit_sound).play()
				self.hit_points -= 1
				self.invulnerability_time = self.hit_interval
				if self.hit_points == 0:
					self.kill()
				else:
					self.flash_counter = self.hit_flash_time
					self.animation = self.hurt_animation
			# Prevent the player from rebounding several frames in a row
			if was_flashing:
				player.rebound_boss()
		else:
			player.damage(self.x)
	
	def update(self):
		if self.hit_points > 0:
			self.invulnerability_time -= 1
			self.flash_counter -= 1
			if self.flash_counter == 0:
				self.animation = self.normal_animation
			# Reset the boss's speeds so that the movement patterns can be applied to determine the
			# boss's speed
			self.x_speed = 0
			self.y_speed = 0
			for pattern in self.movement_patterns:
				pattern(self)
			for weapon in self.weapons:
				weapon.track_boss(self)
		else:
			# When the boss leaves the screen, end the level
			if not self.level.camera.rect.collidepoint(self.x, self.y):
				self.level.end()
			for explosion in self.explosions:
				explosion.update()
			# Every explosion_frequency frames, create an explosion at a random location
			# (the position stored is that of the top-left corner of the explosion)
			if self.explosion_counter % self.explosion_frequency == 0:
				explosion = Animation(frames=self.explosion_sprites, interval=9, vanishing=True)
				x = random.uniform(self.rect.left, self.rect.right)
				y = random.uniform(self.rect.top, self.rect.bottom)
				self.explosions[explosion] = (x - 19, y - 19)
				get_sound(self.explosion_sound).play()
			self.explosion_counter += 1
		self.flipped = self.animation.flip_x = self.x_speed > 0
		with self.level.move(self):
			self.x += self.x_speed
			self.y += self.y_speed
	
	on_ceiling_collision = on_left_wall_collision = on_right_wall_collision = on_floor_collision
	
	def draw(self, surface, camera, rem_time):
		super().draw(surface, camera, rem_time)
		if self.hit_points == 0:
			for explosion, position in self.explosions.items():
				explosion.draw(surface, (position[0] - camera.x, position[1] - camera.y))
	
	def kill(self):
		self.ignore_physics = True
		self.explosion_counter = 0
		self.explosions = {}
		self.x_speed = self.escape_speed
		self.y_speed = 0
		# Remove all weapons
		for weapon in self.weapons:
			self.level.remove(weapon)

class HoverLeftRight:
	"""A type of movement pattern that causes the boss to move back and forth horizontally
	within an area."""
	speed = 2.5
	
	def __init__(self, left_limit, right_limit):
		self.left_limit = left_limit
		self.right_limit = right_limit
		self.x_speed = self.speed
	
	def speed_up(self, factor):
		"""Speeds up the boss's movement by the given factor."""
		self.x_speed *= factor
	
	def __call__(self, boss):
		
		if (boss.x > self.right_limit and self.x_speed > 0) or (boss.x < self.left_limit and self.x_speed < 0):
			self.x_speed = -self.x_speed
		boss.x_speed += self.x_speed

class BounceUpDown:
	"""A type of movement pattern that causes the boss to bounce up and down atop a
	certain Y coordinate (usually the position of the ground)."""
	gravity = 0.2
	max_speed = 6
	
	def __init__(self, lower_limit):
		self.lower_limit = lower_limit
		self.y_speed = 0
	
	def __call__(self, boss):
		self.y_speed += self.gravity
		if boss.y > self.lower_limit and self.y_speed > 0:
			self.y_speed = min(self.y_speed, self.max_speed)
			self.y_speed *= -1
		boss.y_speed += self.y_speed

## Boss weapons

class BossWeapon(BlockSpriteObject, MovingObject):
	"""All boss weapons damage the player whenever touched, by default.
	
	The boss attribute on subclasses of this is set by Boss.attach."""
	def on_floor_collision(self, player):
		player.damage(self.x)
	
	def update(self):
		pass
	
	on_ceiling_collision = on_left_wall_collision = on_right_wall_collision = on_floor_collision

DRILL_X_OFFSET = 40

class Drill(BossWeapon):
	"""A static drill that is attached to the front of the Eggpod."""
	sprite = StaticFrame(image(rel("boss_art/drill.png"), "global"))
	flipped_sprite = StaticFrame(image(rel("boss_art/drill_flipped.png"), "global"))
	
	def __init__(self):
		self.animation = self.sprite
		super().__init__(0, 0, layer=1)
	
	def track_boss(self, boss):
		with self.level.move(self):
			if boss.flipped:
				self.x = boss.x + DRILL_X_OFFSET
				self.animation = self.flipped_sprite
			else:
				self.x = boss.x - DRILL_X_OFFSET
				self.animation = self.sprite
			self.y = boss.y
			
RING_DIAMETER = math.sqrt(128)
EGGPOD_BOTTOM_OFFSET = 25

class WreckingBall(BossWeapon):
	"""A wrecking ball that rotates in a circle around the Eggpod, visually attached by a chain."""
	
	animation = StaticFrame(image(rel("boss_art/wrecking_ball.png"), "global"))
	chain_link = StaticFrame(image(rel("builtins_art/ring/spin/1.png"), "global"))
	
	def __init__(self, radius, angular_velocity):
		"""Creates a new wrecking ball, specifying its radius of rotation and angular velocity
		(in radians per frame)."""
		self.radius = radius
		self.angular_velocity = angular_velocity
		self.angle = random.uniform(0, 2 * math.pi)
		super().__init__(0, 0, layer=1)
	
	def update(self):
		self.angle += self.angular_velocity
	
	def track_boss(self, boss):
		with self.level.move(self):
			self.x = boss.x + self.radius * math.cos(self.angle)
			self.y = boss.y + EGGPOD_BOTTOM_OFFSET + self.radius * math.sin(self.angle)
	
	def draw(self, surface, camera, rem_time):
		num_links = math.floor(self.radius / RING_DIAMETER)
		x = self.x
		y = self.y
		# Just shorthand to make the following prettier
		k = self.chain_link.current_image.get_width()
		dy = RING_DIAMETER * math.sin(self.angle)
		dx = RING_DIAMETER * math.cos(self.angle)
		for i in range(num_links):
			self.chain_link.draw(surface, (x - camera.x - k, y - camera.y - k))
			x -= dx
			y -= dy
		# Draw the ball last so it shows up on top of the rings
		super().draw(surface, camera, rem_time)

hammer_rotations = {}

HAMMER_X_OFFSET = 30

class Hammer(BossWeapon):
	"""A hammer that slams down in front of the boss periodically."""
	strike_interval = 90
	strike_time = 30
	max_angle = math.pi / 4
	
	hammer_sprite = image(rel("boss_art/hammer.png"), "global")
	
	def __init__(self):
		self.angle = self.max_angle
		self.strike_counter = 0
		self.strike_interval_counter = self.strike_interval
		self.animation = StaticFrame(self.hammer_sprite)
		self.flipped = False
		# An image of the rotated hammer
		self.current_sprite = transform.rotate(self.animation.current_image, math.degrees(self.angle))
		# -1 if slamming down, 1 if going back up, 0 if waiting
		self.direction = 0
		super().__init__(0, 0, layer=1)
	
	@property	
	def collision_mask(self):
		return self.current_sprite
	
	def update(self):
		# Rotate the sprite and collision mask so that the hammer's collision matches its
		# position on screen
		self.current_sprite = transform.rotate(self.animation.current_image, math.degrees(self.angle))
		if self.flipped:
			self.current_sprite = transform.flip(self.current_sprite, True, False)
		with self.level.move(self):
			self.width = self.current_sprite.get_width()
			self.height = self.current_sprite.get_height()
		# Strike!
		self.strike_interval_counter -= 1
		if self.strike_interval_counter == 0:
			self.strike_counter = self.strike_time
			self.direction = -1
		# If the hammer is waiting, then self.direction is 0 and it will not move
		self.angle += self.direction * self.max_angle / self.strike_time
		self.strike_counter -= 1
		# At the end of the down strike, move back up; at the end of that, start counting
		# down towards the next one
		if self.strike_counter == 0:
			if self.direction == 1:
				self.direction = 0
				self.strike_interval_counter = self.strike_interval
			elif self.direction == -1:
				self.direction = 1
				self.strike_counter = self.strike_time
	
	def track_boss(self, boss):
		self.flipped = not boss.flipped
		with self.level.move(self):
			self.x = boss.x + (HAMMER_X_OFFSET if boss.flipped else -HAMMER_X_OFFSET)
			self.y = boss.y
	
	def draw(self, surface, camera, rem_time):
		x = self.x - camera.x - self.current_sprite.get_width() / 2
		y = self.y - camera.y - self.current_sprite.get_height() / 2
		surface.blit(self.current_sprite, (x, y))