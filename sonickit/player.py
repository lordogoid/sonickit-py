import math
from .animation import Animation, StaticFrame, gen_frame_sequence
from pygame import Rect
from pygame.transform import flip, rotate
from . import logging
from .resources import image, rel, sound, get_sound
from .physics import quadrants
from .builtins import BouncingRing
from . import keyboard, controls

QUADRANT_SWITCH_THRESHOLD = math.pi / 4
QUADRANT_SWITCH_ADJUSTMENT = math.pi / 2

class Player:
	width = 20
	height = 40
	# How much the player's height decreases when rolling
	roll_height_diff = 10
	
	# The horizontal and vertical sensor lengths
	hl = 11
	vl = 40
	
	# The horizontal sensor offset
	ho = 4
		
	def __init__(self):
		self.x = 0
		self.y = 0
		self.lives = 3
		self.emeralds = set()
		self.score = 0
		self.hw = int(self.width / 2 - 1)
		self.remaining_invincibility_time = 0
		self.no_ring_time_remaining = 0
		self.create_animations()
		self.set_initial_state()
		
	def set_initial_state(self):
		"""Resets the player's movement parameters to what will usually make sense at the beginning of a level."""
		self.angle = 0
		self.rings = 0
		self.attached_platform = None
		self.facing_left = False
		self.holding_jump_button = False
		self.jumping = False
		self.shielded = False
		self.quadrant = quadrants["air"]
		self.mode = "air"
		self.animation = self.animations["damage"]
		self.spinning = False
		self.balancing = False
		self.braking = 0
		self.speed = 0
		self.x_speed = 0
		self.y_speed = 0
		# Number of frames until the horizontal control lock is released (0 if disabled)
		self.control_lock_time = 0
		
	def attach_to_platform(self, platform):
		"""Causes the player to follow the movement of the given platform until they jump out
		or fall off."""
		# Record the number of the frame where we became attached to the platform, so we only
		# follow its movement if we made contact with it on the same frame
		# Using the keyboard module's frame counter for that is a bit of a hack, but it's
		# already there and it works.
		self.attachment_time = keyboard.frame_counter
		self.attached_platform = platform
		
	shield_anim = image(rel("art/shield.png"), "global")
	
	def create_animations(self):
		"""Creates this player's animation objects. They must be distinct from other
		players' objects, since they hold the animation state."""
		self.animations = dict(
			walking = Animation(self.walking_anim, interval = 8),
			running = Animation(self.running_anim, interval = 8),
			spinning = Animation(self.spinning_anim, interval = 8),
			standing = Animation(self.standing_anim, interval = -1),
			crouching = Animation(self.crouching_anim, interval = 8, restart_index = None),
			looking_up = Animation(self.looking_up_anim, interval = 8, restart_index = None),
			springing = StaticFrame(self.springing_anim),
			balancing = Animation(self.balancing_anim, interval = 16),
			braking = Animation(self.braking_anim, interval = 8),
			damage = Animation(self.damage_anim, interval = 20),
			end_level = Animation(self.end_level_anim, interval = 15, restart_index = 1),
			death = Animation(self.death_anim, interval = -1),
			shield = StaticFrame(self.shield_anim)
		)
		self.animation = self.animations["standing"]
	
	jump_sound = sound(rel("sfx/player/jump.wav"), "global")
	brake_sound = sound(rel("sfx/player/brake.wav"), "global")
	ring_loss_sound = sound(rel("sfx/player/ring_loss.wav"), "global")
	damage_sound = sound(rel("sfx/player/damage.wav"), "global")
	roll_sound = sound(rel("sfx/player/roll.wav"), "global")
	spindash_charge_sound = sound(rel("sfx/player/spindash_charge.wav"), "global")
	spindash_release_sound = sound(rel("sfx/player/spindash_release.wav"), "global")
		
	@property
	def can_get_rings(self):
		"""Returns True if the player can currently capture rings."""
		return self.no_ring_time_remaining == 0
	
	outer_bouncing_ring_speed = 4
	inner_bouncing_ring_speed = 2
	no_ring_time = 64
	
	def damage(self, hazard_x_position):
		"""Deals one instance of damage to the player. If it has at least one ring, all rings
		are lost and it becomes temporarily invincible, bouncing away from the hazard.
		Up to 32 rings bounce away in two circular waves from the player.
		Otherwise, it dies immediately (see kill()).
		If the player has a shield, the shield is lost instead of the rings.
		
		Has no effect if the player is invincible or dead.
		
		hazard_x_position should be the X coordinate of the center of the object that damaged the player.
		
		Returns true if the player actually was damaged."""
		if self.remaining_invincibility_time > 0 or self.mode == "dead":
			return False
		if self.shielded or self.rings > 0:
			self.y_speed = self.damaged_y_speed
			# The player bounces away from the center of the hazard they hit. If it is
			# exactly over the center, it bounces forward and not directly upward.
			direction = self.x - hazard_x_position
			if direction == 0:
				direction = 1
			self.x_speed = math.copysign(self.damaged_x_speed, direction)
			self.mode = "damaged"
			self.quadrant = quadrants["air"]
			self.animation = self.animations["damage"]
			self.attached_platform = None
			self.remaining_invincibility_time = self.temporary_invincibility_time
			if self.shielded:
				self.shielded = False
				get_sound(self.damage_sound).play()
			else:
				# Spawn the rings
				spawned_rings = min(self.rings, 32)
				logging.bouncing_rings_spawned(spawned_rings)
				angle = -math.pi / 2 * 1.1
				speed = self.outer_bouncing_ring_speed
				self.no_ring_time_remaining = self.no_ring_time
				for i in range(spawned_rings):
					x_speed = math.cos(angle) * speed
					y_speed = math.sin(angle) * speed
					if i % 2 == 1:
						x_speed = -x_speed
						angle += math.pi / 8
					self.level.add(BouncingRing(x=self.x, y=self.y, x_speed=x_speed, y_speed=y_speed, layer=self.layer))
					if i == 15:
						speed = self.inner_bouncing_ring_speed
						angle = -math.pi / 2 * 1.1
				self.rings = 0
				get_sound(self.ring_loss_sound).play()
		else:
			self.kill()
			logging.player_died_damage()
		return True
			
	def kill(self):
		"""Kills the player, decreasing its life count by 1. If the life count is still above 0,
		the player respawns at the start of the level."""
		self.x_speed = 0
		self.y_speed = -self.death_speed
		self.mode = "dead"
		self.quadrant = quadrants["air"]
		self.animation = self.animations["death"]
		self.level.camera.frozen = True
		self.lives -= 1
		if self.spinning or self.mode == "crouching":
			self.grow_y()
		# Make sure the player is drawn above everything else
		self.layer = 2000
		get_sound(self.damage_sound).play()
	
	def draw(self, surface, camera, rem_time):
		"""Draws the player."""
		# Interpolate the position
		ix = self.old_x * (1 - rem_time) + self.x * rem_time
		iy = self.old_y * (1 - rem_time) + self.y * rem_time
		image = self.animation.current_image
		if self.facing_left:
			image = flip(image, True, False)
		if not self.spinning:
			image = rotate(image, -math.degrees(self.angle))
		surface.blit(image, (ix - image.get_width() / 2 - camera.left,
				iy - image.get_height() / 2 - camera.top))
		if self.shielded:
			shield = self.animations["shield"].current_image
			self.animations["shield"].draw(surface, (ix - shield.get_width() / 2 - camera.left,
			iy - shield.get_height() / 2 - camera.top))
		
	def save_pos(self):
		self.old_x = self.x
		self.old_y = self.y
		
	def set_pos(self, x, y, layer):
		"""Sets the player's position to (x, y) on the given layer."""
		self.x = x
		self.y = y
		self.layer = layer
		self.save_pos()
		
	@property
	def airborne(self):
		return self.mode in {"air", "damaged", "dead"}
	
	@property
	def px(self):
		"""This object's integer X coordinate."""
		return int(self.x)
	
	@property
	def py(self):
		"""This object's integer Y coordinate."""
		return int(self.y)
	
	def set_angle(self, angle):
		"""Sets the player's angle and quadrant values according to the given
		measured angle (which is relative to the sensor angles)"""
		if angle > QUADRANT_SWITCH_THRESHOLD:
			angle -= QUADRANT_SWITCH_ADJUSTMENT
			self.quadrant = quadrants[self.quadrant.right]
		elif angle < -QUADRANT_SWITCH_THRESHOLD:
			angle += QUADRANT_SWITCH_ADJUSTMENT
			self.quadrant = quadrants[self.quadrant.left]
		# Adjust the angle to the current quadrant
		self.angle = self.quadrant.base + angle
	
	def shift_height(self, shift):
		""" Moves the player shift pixels away from the ground."""
		normal = self.quadrant.base - math.pi / 2
		self.x += math.cos(normal) * shift
		self.y += math.sin(normal) * shift
	
	def adjust_height(self, dist):
		"""Adjusts the player's height according to the currently measured distance to the ground.
		Does nothing if the player is in the air."""
		self.shift_height(self.height / 2 - dist)
		
	def adjust_width(self, dist, left):
		"""Adjusts the player's position along the ground when colliding with walls.
		left should be True if the collision is with a left wall, False otherwise.
		"""
		shift = dist - self.width / 2
		if left:
			shift = -shift
		self.x += math.cos(self.quadrant.base) * shift
		self.y += math.sin(self.quadrant.base) * shift
		
	def should_hit_floor(self, dist, obj_y_speed = 0):
		return not self.airborne or (self.y_speed > 0 or obj_y_speed < 0) and self.height / 2 > dist
	
	def should_hit_ceiling(self, dist, obj_y_speed = 0):
		return (self.y_speed < 0 or obj_y_speed > 0) and self.height / 2 > dist
	
	def should_hit_left_wall(self, dist, obj_x_speed = 0):
		return (self.x_speed if self.airborne else self.speed) > 0 or obj_x_speed < 0
	
	def should_hit_right_wall(self, dist, obj_x_speed = 0):
		return (self.x_speed if self.airborne else self.speed) < 0 or obj_x_speed > 0
	
	def fall(self):
		self.go_airborne()
		
	def collide_walls(self, ah, bh, ch, dh, eh, fh, gh, hh, ih, jh, kh, lh, xh = None):
		"""If the player is partially inside a wall, pushes it out of the wall by moving it
		horizontally as far as needed to prevent the collision. This is so that a player cannot
		become wedged inside a wall by moving too fast and crossing the wall in one frame.
		
		This function takes as input the values of the 13 collision sensors used in Level.handle_collision."""
		if self.mode == "dead":
			return
		inf = float("inf")
		if self.airborne:
			if eh != inf:
				self.adjust_width(eh, False)
				if self.x_speed > 0:
					self.x_speed = 0
			elif gh != inf:
				self.adjust_width(gh, True)
				if self.x_speed < 0:
					self.x_speed = 0
		else:
			if (eh != inf or fh != inf):
				self.adjust_width(min(eh, fh), False)
				if self.speed > 0:
					self.control_lock_time = 1
					self.speed = 0
			elif (gh != inf or hh != inf):
				self.adjust_width(min(gh, hh), True)
				if self.speed < 0:
					self.control_lock_time = 1
					self.speed = 0
	
	def collide_ground(self, ah, bh, ch, dh, eh, fh, gh, hh, ih, jh, kh, lh, xh = None):
		"""According to the shape of the ground below the player's feet, adjusts its angle, offset from
		the ground, and also determines the ground speed if landing on this frame.
		
		This function takes as input the values of the 13 collision sensors used in Level.handle_collision."""
		if self.mode == "dead":
			return
		dist = min(ah, bh)
		inf = float("inf")
		if self.airborne:
			if (ah != inf or bh != inf) and self.should_hit_floor(dist):
				# Land
				was_damaged = self.mode == "damaged"
				self.mode = "ground"
				self.quadrant = quadrants["floor"]
				self.jumping = False
				self.holding_jump_button = False
				if ah != inf and bh != inf:
					self.set_angle(math.atan2(bh - ah, 2 * self.hw))
				# Handle very steep slopes
				elif ih != inf and jh != inf:
					self.quadrant = quadrants["right_wall"]
					self.angle = math.atan2(jh - ih, 2 * self.hw) + self.quadrant.base
					dist = min(ih, jh)
				elif kh != inf and lh != inf:
					self.quadrant = quadrants["left_wall"]
					self.angle = math.atan2(lh - kh, 2 * self.hw) + self.quadrant.base
					dist = min(kh, lh)
				else:
					self.angle = 0
				self.adjust_height(dist)
				if self.spinning:
					self.unroll()	
				if was_damaged:
					self.speed = 0
				else:
					self.speed = (math.sqrt(self.x_speed ** 2 + self.y_speed ** 2) *
				             math.cos(math.atan2(self.y_speed, self.x_speed) - self.angle))
			# Ceiling collision
			if (ch <= self.height / 2 or dh <= self.height / 2) and self.should_hit_ceiling(min(ch, dh)):
				self.y_speed = 0
		else:
			self.balancing = False
			# Handle floor collision
			if ah != inf and bh != inf:
				self.adjust_height(dist)
				self.set_angle(math.atan2(bh - ah, 2 * self.hw))
			# If on the edge of the platform, keep the last angle that was measured
			elif ah != inf or bh != inf:
				self.balancing = xh == inf
				self.adjust_height(dist)
			else:
				self.fall()
		
	gravity = 0.21875
	accel = 0.046875
	decel = 0.5
	friction = 0.046875
	slope_factor = 0.125
		
	air_accel = 0.09375
	# The X speed below which air drag is not applied
	air_drag_threshold = 0.125
	air_drag_factor = 0.96875
		
	rolling_friction = 0.023475
	rolling_decel = 0.125
		
	unroll_speed = 0.5
	min_roll_speed = 1.03125
	min_wall_speed = 2.5
	speed_cap = 6
	y_speed_cap = 16
	
	damaged_y_speed = -4
	damaged_x_speed = 2
	damaged_gravity = 0.1875
	temporary_invincibility_time = 120
		
	jump_speed = 6.5
	reduced_jump_speed = 4
	death_speed = 7
		
	slide_time = 30
	
	spindash_power_step = 2
	spindash_power_cap = 8
	spindash_base_speed = 8
		
	def apply_friction(self, friction):
		"""Slows down the player's movement by at most the given amount."""
		self.speed -= math.copysign(min(abs(self.speed), friction), self.speed)
		
	def set_animation_speed(self, max_interval, speed):
		"""Adjusts the speed of the current animation to match the player's current movement speed.
		Has no effect if the player is not in the walking, running, or spinning animation."""
		if (self.animation is self.animations["walking"] or self.animation is self.animations["running"] or
		   self.animation is self.animations["spinning"]):
			self.animation.interval = max(max_interval - int(abs(speed)), 1)
		
	# The speed past which the player enters the running animation.
	running_animation_threshold = 6
	
	# The speed beyond which braking triggers the braking animation.
	braking_animation_threshold = 4.5
	
	def brake(self, sign):
		self.braking = sign
		self.animation = self.animations["braking"]
		get_sound(self.brake_sound).stop()
		get_sound(self.brake_sound).play()
			
	def move_walk(self):
		"""Handles directional movement on ground."""
		if keyboard.pressed(controls.left):
			if self.speed <= 0:
				self.braking = 0
				if self.speed > -self.speed_cap:
					self.speed = max(self.speed - self.accel, -self.speed_cap)
				self.facing_left = True
			else:
				if self.speed >= self.braking_animation_threshold:
					self.brake(1)
				self.speed -= self.decel
		elif keyboard.pressed(controls.right):
			if self.speed >= 0:
				self.braking = 0
				if self.speed < self.speed_cap:
					self.speed = min(self.speed + self.accel, self.speed_cap)
				self.facing_left = False
			else:
				if self.speed <= -self.braking_animation_threshold:
					self.brake(-1)
				self.speed += self.decel
		else:
			# Apply running friction
			self.apply_friction(self.friction)
			
	def move_roll(self):
		"""Handles directional movement when rolling on ground."""
		# When rolling, the player is only allowed to decelerate
		if self.speed > 0 and keyboard.pressed(controls.left):
			self.speed -= self.rolling_decel
		elif self.speed < 0 and keyboard.pressed(controls.right):
			self.speed += self.rolling_decel
		self.apply_friction(self.rolling_friction)
		# Unroll if going too slowly
		if abs(self.speed) < self.unroll_speed:
			self.unroll()
	
	def move_air(self):
		"""Handles gravity and directional movement on air."""
		# Apply air drag if going upwards at at most 4 pixels per frame
		if -4 < self.y_speed < 0 and abs(self.x_speed) >= self.air_drag_threshold:
			self.x_speed *= self.air_drag_factor
		# If the player releases the jump button during a jump, reduce the jump height
		if self.holding_jump_button and not keyboard.pressed(controls.jump):
			self.y_speed = max(self.y_speed, -self.reduced_jump_speed)
			self.holding_jump_button = False
		gravity = self.gravity if self.mode != "damaged" else self.damaged_gravity
		self.y_speed = min(self.y_speed + gravity, self.y_speed_cap)
		self.x += self.x_speed
		self.y += self.y_speed
		# The player cannot control its movement when bouncing due to being damaged or dead
		if self.mode not in {"damaged", "dead"}:
			if keyboard.pressed(controls.left):
				self.facing_left = True
				if self.x_speed > -self.speed_cap:
					self.x_speed = max(self.x_speed - self.air_accel, -self.speed_cap)
			elif keyboard.pressed(controls.right):
				self.facing_left = False
				if self.x_speed < self.speed_cap:
					self.x_speed = min(self.x_speed + self.air_accel, self.speed_cap)
		self.set_animation_speed(5, self.x_speed)
		
	def go_airborne(self):
		"""Sets the player's state to airborne and sets collision detection sensors appropriately."""
		self.mode = "air"
		self.quadrant = quadrants["air"]
		self.attached_platform = None
		
	def jump(self):
		"""Causes the player to jump (in a spinning state)."""
		if not self.spinning:
			self.roll()
		self.go_airborne()
		self.x_speed += self.jump_speed * math.sin(self.angle)
		self.y_speed -= self.jump_speed * math.cos(self.angle)
		self.holding_jump_button = True
		self.jumping = True
		logging.player_jumped()
		get_sound(self.jump_sound).play()
		
	def bounce_upward(self):
		"""Causes the player to bounce upward."""
		if keyboard.pressed(controls.jump) or not self.jumping:
			self.y_speed = -self.y_speed
		else:
			self.y_speed = max(-2.5, -self.y_speed)
	
	def rebound_enemy(self, y):
		"""Causes the player to rebound as if colliding with an enemy. Takes the
		Y position of the object it is rebounding from as a parameter.
		
		Has no effect if the player is not airborne."""
		if not self.airborne:
			return
		if self.y > y or self.y_speed < 0:
			# Attenuate speed slightly
			self.y_speed -= math.copysign(1, self.y_speed)
		else:
			self.bounce_upward()
			
	def rebound_monitor(self, y):
		"""Causes the player to rebound as if colliding with a monitor. Takes the Y position
		of the object it is rebounding from as a parameter.
		
		Has no effect if the player is not airborne or is rising and is above the object's position."""
		if not self.airborne:
			return
		if self.y_speed > 0:
			self.bounce_upward()
		elif self.y > y:
			self.y_speed = 0
	
	def rebound_boss(self):
		"""Causes the player to rebound as if colliding with a boss."""
		self.x_speed *= -1
		self.y_speed *= -1
		self.speed *= -1
		
	def shrink_y(self):
		"""Makes the player shrink by rollHeightDiff pixels towards the ground."""
		self.height -= self.roll_height_diff
		self.shift_height(-self.roll_height_diff / 2)
		
	def grow_y(self):
		"""Reverses the action of shrinkY."""
		self.height += self.roll_height_diff
		self.shift_height(+self.roll_height_diff / 2)
		
	def roll(self):
		"""Causes the player to roll into a ball (also lowers its height, as when crouching)"""
		self.spinning = True
		self.braking = 0
		self.animation = self.animations["spinning"]
		# Ensure that the player's height is not reduced again when going from crouching to spinning
		# (the opposite isn't a problem because the player unrolls first)
		if self.mode == "crouching":
			self.mode = "ground"
			return
		self.shrink_y()
	
	def unroll(self):
		self.spinning = False
		self.animation = self.animations["standing"]
		self.grow_y()
		
	def crouch(self):
		self.mode = "crouching"
		self.animation = self.animations["crouching"]
		self.animation.reset()
		self.shrink_y()
		self.speed = 0
	
	def uncrouch(self):
		self.mode = "ground"
		self.animation = self.animations["standing"]
		self.grow_y()
		
	def look_up(self):
		self.mode = "looking_up"
		self.speed = 0
		self.animation = self.animations["looking_up"]
		self.speed = 0
		self.animation.reset()
	
	def stop_look_up(self):
		self.mode = "ground"
		self.animation = self.animations["standing"]
			
	def update(self):
		self.animation.update()
		# If the level is over, there is nothing more to be done
		if self.animation is self.animations["end_level"]:
			return
		if self.remaining_invincibility_time > 0:
			self.remaining_invincibility_time -= 1
		if self.no_ring_time_remaining > 0:
			self.no_ring_time_remaining -= 1
		# Follow the movement of the moving platform the player is on, if any
		if self.mode in {"ground", "crouching", "spindashing", "looking_up"}:
			self.x_speed = self.speed * math.cos(self.angle)
			self.y_speed = self.speed * math.sin(self.angle)
			logging.measure_speed(self.speed)
			if self.attached_platform is not None and self.attachment_time == keyboard.frame_counter:
				self.x_speed += self.attached_platform.x_speed
				self.y_speed += self.attached_platform.y_speed
			self.x += self.x_speed
			self.y += self.y_speed
		if self.airborne:
			self.move_air()
			if self.animation is self.animations["springing"] and self.y_speed >= 0:
				self.animation = self.animations["walking"]
			if self.mode == "dead":
				return
		elif self.mode == "ground":
			if self.control_lock_time == 0 and self.mode != "damaged":
				if self.spinning:
					self.move_roll()
				else:
					self.move_walk()
			else:
				self.control_lock_time -= 1
				self.apply_friction(self.rolling_friction if self.spinning else self.friction)
			# Set the proper animation
			if not self.spinning or self.braking:
				if abs(self.speed) < 0.05:
					self.animation = self.animations["standing" if not self.balancing else "balancing"]
				elif abs(self.speed) < self.running_animation_threshold:
					self.animation = self.animations["walking"]
				else:
					self.animation = self.animations["running"]
			if not self.braking:
				self.set_animation_speed(8, self.speed)
			# Check if the braking hasn't ended yet
			elif self.speed * self.braking > 0:
				self.animation = self.animations["braking"]
			self.speed += self.slope_factor * math.sin(self.angle)
			# Fall off of walls and ceilings when going too slowly
			if self.quadrant is not quadrants["floor"] and abs(self.speed) < self.min_wall_speed:
				self.fall()
				self.control_lock_time = self.slide_time
				return
			# Rolling
			if keyboard.pressed(controls.down) and not self.spinning:
				if abs(self.speed) >= self.min_roll_speed:
					self.roll()
					logging.player_rolled()
					get_sound(self.roll_sound).play()
				else:
					self.crouch()
			if keyboard.pressed(controls.up) and abs(self.speed) < 0.05:
				self.look_up()
			if keyboard.pressed(controls.jump) == 1:
				self.jump()
		elif self.mode == "crouching":
			if not keyboard.pressed(controls.down):
				self.uncrouch()
			if keyboard.pressed(controls.jump) == 1:
				self.mode = "spindashing"
				self.spindash_power = 0
				self.spinning = True
				self.animation = self.animations["spinning"]
				logging.player_began_spindash(self.level.timer)
				get_sound(self.spindash_charge_sound).play()
		elif self.mode == "looking_up":
			if not keyboard.pressed(controls.up):
				self.stop_look_up()
			if keyboard.pressed(controls.jump) == 1:
				self.jump()
		elif self.mode == "spindashing":
			if keyboard.pressed(controls.jump) == 1:
				self.spindash_power = min(self.spindash_power + self.spindash_power_step, self.spindash_power_cap)
				logging.player_charged_spindash()
				get_sound(self.spindash_charge_sound).stop()
				get_sound(self.spindash_charge_sound).play()
			self.spindash_power *= self.air_drag_factor
			# Once the down button is released, launch the player forward
			if not keyboard.pressed(controls.down):
				multiplier = -1 if self.facing_left else 1
				self.speed = multiplier * (self.spindash_base_speed + math.floor(self.spindash_power) / 2)
				self.mode = "ground"
				logging.player_ended_spindash(self.level.timer)
				get_sound(self.spindash_release_sound).play()
			
	
	def on_end_level(self):
		"""Stops the player, locks the controls and starts the end-of-level animation."""
		if self.spinning:
			self.unroll()
		self.animation = self.animations["end_level"]
		logging.level_ended(self.level.timer)
	
	@property	
	def rect(self):
		width, height = self.width, self.height
		if self.quadrant.name in {"right_wall", "left_wall"}:
			width, height = height, width
		return Rect(self.x - width / 2, self.y - height / 2, width, height)
		
	@property
	def collision_rect(self):
		total_width, total_height = self.width + 10, self.height + 10
		if self.quadrant.name in {"right_wall", "left_wall"}:
			total_width, total_height = total_height, total_width
		return Rect(self.x - total_width / 2, self.y - total_height / 2, total_width, total_height)