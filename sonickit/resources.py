import os
import traceback
from collections import Iterable, defaultdict
from threading import Thread
import pygame.image
from pygame.mixer import Sound

class Pool:
	"""This class represents a resource pool, which can be used to load images and sounds."""
	def __init__(self):
		self.images = {}
		self.sounds = {}
		
	def load_sync(self):
		"""Loads all of the resources in this pool. Returns only when the loading is done."""
		for name in self.images:
			self.images[name] = pygame.image.load(name).convert_alpha()
		for name in self.sounds:
			self.sounds[name] = Sound(name)
	
	def load(self, callback):
		"""Loads all of the resources listed in this pool. callback will be called when this is done."""
		def loader():
			self.load_sync()
			callback()
		Thread(target = loader).start()

pools = defaultdict(Pool)

def register(res, name, res_type):
	"""Registers a resource or an iterable of resources into the resource pool with the given name, creating
	it if it doesn't exist yet. type should be "images" or "musics".
	Returns res."""
	if name not in pools:
		pools[name] = Pool()
	pool = getattr(pools[name], res_type)
	if isinstance(res, Iterable) and not isinstance(res, str):
		for item in res:
			pool[item] = None
	else:
		pool[res] = None
	return res
	
default_pool = None
			
def image(res, pool_name = None):
	"""Registers an image or several into the pool with the given name. If the pool name is not given, the default pool is assumed."""
	return register(res, pool_name or default_pool, "images")

def sound(res, pool_name = None):
	"""Registers a sound or several into the pool with the given name. If the pool name is not given, the default pool is assumed."""
	return register(res, pool_name or default_pool, "sounds")
	
def get_image(res):
	"""Returns the image with the name res, looking first in the level-specific pool
	and then in the global one."""
	if res in specific_pool.images:
		return specific_pool.images[res]
	else:
		return global_pool.images[res]

def get_sound(res):
	"""Returns the sound with the name res, looking first in the level-specific pool
	and then in the global one."""
	if res in specific_pool.sounds:
		return specific_pool.sounds[res]
	else:
		return global_pool.sounds[res]
	
def load(name, callback):
	"""Loads the pool with the given name, calling callback when it has loaded.
	Returns immediately. If the pool is "global", it is set to be the global one,
	otherwise it takes up the specific slot."""
	pools[name].load(callback)
	if name != "global":
		global specific_pool
		specific_pool = pools[name]
		
def load_global():
	"""Loads the global pool synchronously. This should generally be used before starting the game."""
	global_pool.load_sync()
			
def set_default_pool(name):
	"""Sets the default pool to name."""
	global default_pool
	default_pool = name
	
def rel(path):
	"""Converts a path relative to the file where this function is called to a path
	relative to the current working directory (or an absolute path, if available).
	Useful for referencing files relative to an imported file."""
	return os.path.join(os.path.dirname(traceback.extract_stack(limit=2)[-2][0]), path)
		
"""The pool to be used for resources used on all levels. Normally, should be only loaded once."""
global_pool = Pool()
"""The pool to be used for resources used for only one level or screen."""
specific_pool = Pool()

pools["global"] = global_pool