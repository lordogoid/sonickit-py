# For all currently-pressed keys, holds the number of the frame in which it was pressed.
key_table = {}
# Holds keys that were pressed and released between two frames.
del_after_next_frame = set()

frame_counter = 0

def keydown(key):
	"""Registers the pressing of the given key."""
	key_table[key] = frame_counter
	if key in del_after_next_frame:
		del_after_next_frame.remove(key)

def keyup(key):
	"""Registers the release of the given key; if the key is released between two frames, it is only removed from the key table after the next frame, otherwise it is removed immediately."""
	if key not in key_table:
		return
	if key_table[key] == frame_counter:
		del_after_next_frame.add(key)
	else:
		del key_table[key]

def del_tapped_keys():
	"""Deletes all keys that were tapped between the last two frames from the key table."""
	for key in del_after_next_frame:
		if key in key_table:
			del key_table[key]
		
def update():
	global frame_counter
	"""Updates the internal frame counter."""
	frame_counter += 1

def keypress_duration(key):
	"""Returns the number of frames for which key has been held or pressed. If a key is tapped, this will return 1 in the following frame.
	If the key is not pressed, returns False."""
	if key in key_table:
		return frame_counter - key_table[key]
	else:
		return False
		
pressed = keypress_duration