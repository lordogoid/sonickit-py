"""The store module provides two simple functions to store and load objects to and from files."""

import ast
import pprint

def fullname(_class):
	"""Returns the fully-qualified name of a class, including its containing module."""
	return "{}.{}".format(_class.__module__, _class.__name__)

def load(fname):
	"""Returns the Python object represented by the contents of the file with the given name."""
	with open(fname) as file:
		return ast.literal_eval(file.read())

def dump(fname, obj):
	"""Writes a representation of obj to the file named fname. This representation can later
	be loaded using load()."""
	with open(fname, "w") as file:
		file.write(pprint.pformat(obj))