import sys
import math
from contextlib import contextmanager
from collections import defaultdict
from pygame import draw, Rect, Surface, display
from . import resources, text, physics, aux, music
from .resources import sound, get_sound, rel
from .object import PhysicsObject, GroundFollowingObject, MovingObject
from .background import Background

def shift_amount(rel, max_shift, low, high):
	if rel < low:
		return -min(max_shift, low - rel)
	elif rel > high:
		return min(max_shift, rel - high)
	else:
		return 0

class Camera:
	"""This class implements a camera that works similarly to the classic games."""
	def __init__(self, *, width = None, height = None,
			left_x_border = None, right_x_border = None, top_y_border = None, bottom_y_border = None):
		screen = display.get_surface()
		"""Creates a new camera. The width and height can be specified, as well
		as the limits of the player's location on screen.
		If no size is specified, the size of the view defaults to the size of the screen
		(for this to work, the game window must have been created already).
		If one of the borders is not specified, it defaults to:
		- left x: width // 2 - 8
		- right x: width // 2 + 8
		- top y: height // 2 - 48
		- bottom y: height // 2 + 16"""
		self.x = 0
		self.y = 0
		self.width = width if width is not None else screen.get_width()
		self.height = height if height is not None else screen.get_height()
		# This attribute determines whether the camera should follow the player or not
		self.frozen = False
		self.left_x_border = left_x_border if left_x_border is not None else self.width // 2 - 8
		self.right_x_border = right_x_border if right_x_border is not None else self.width // 2 + 8
		self.top_y_border = top_y_border if top_y_border is not None else self.height // 2 - 48
		self.bottom_y_border = bottom_y_border if bottom_y_border is not None else self.height // 2 + 16
		self.save_pos()
		
	def save_pos(self):
		self.old_x = self.x
		self.old_y = self.y
		
	# The maximum number of pixels the camera is allowed to shift in one frame
	max_shift = 16
	
	def shift(self, player):
		"""Moves the camera's position in order to follow the player.
		(May not keep the player on screen if it is moving too fast)
		
		Has no effect if the camera is frozen (its frozen attribute is set to True)."""
		if self.frozen:
			return
		relx = player.x - self.x
		rely = player.y - self.y
		# Adjust the X position
		self.x += shift_amount(relx, self.max_shift, self.left_x_border, self.right_x_border)
		# Adjust the Y position
		if player.airborne:
			self.y += shift_amount(rely, self.max_shift, self.top_y_border, self.bottom_y_border)
		else:
			y_target = self.height / 2
			if player.mode == "crouching" or player.mode == "ground" and player.spinning:
				y_target += player.roll_height_diff / 2 * math.cos(player.angle)
			diff = rely - y_target
			limit = self.max_shift if player.y_speed > 6 else 6
			if abs(diff) > 0.1:
				self.y += math.copysign(min(abs(diff), limit), diff)
	
	def center(self, player):
		"""Instantaneously moves the camera such that the player is at its center."""
		self.x = player.x - self.width / 2
		self.y = player.y - self.height / 2
		
	@property
	def rect(self):
		return Rect(self.x, self.y, 426, 240)
		
	def interpolated_rect(self, rem_time):
		"""Returns the camera's current interpolated position."""
		return Rect(self.old_x * (1 - rem_time) + self.x * rem_time, self.old_y * (1 - rem_time) + self.y * rem_time, self.width, self.height)

class ObjectGrid:
	"""This class implements a grid which can be used to quickly search for objects
	inside a given rectangle.
	"""
	def __init__(self, block_width = 256, block_height = 256):
		"""Creates a new ObjectGrid with a given block size. Defaults to 256x256 pixel blocks."""
		self.block_width = block_width
		self.block_height = block_height
		self.grid = defaultdict(lambda: defaultdict(list))
	
	def cells_for_rect(self, rect):
		"""Returns an iterator for the (left, top) coordinates of all grid cells that intersect rect."""
		xi, xm = divmod(rect.left, self.block_width)
		yi, ym = divmod(rect.top, self.block_height)
		for i in range(xi, xi + (rect.width + xm) // self.block_width + 1):
			for j in range(yi, yi + (rect.height + ym) // self.block_height + 1):
				yield i, j
	
	def add(self, obj):
		"""Adds obj to the grid. obj must have a rect property, which must be an instance of pygame.Rect.
		It must also have a layer property, with the number of the layer it is in.
		"""
		layer = self.grid[obj.layer]
		for cell in self.cells_for_rect(obj.rect):
			layer[cell].append(obj)
	
	def remove(self, obj):
		"""Removes obj from the grid.
		Note: if obj is not already in the grid, bad things will happen. Don't do that."""
		layer = self.grid[obj.layer]
		for cell in self.cells_for_rect(obj.rect):
			layer[cell].remove(obj)
			
	def layers(self):
		"""Returns an iterator for all existing layers in the grid."""
		return sorted(self.grid.keys())
	
	def objects_in_rect(self, rect, layer=None):
		"""Returns the set of objects in the layer with index layer that intersect
		rect. If layer is not given, searches across all layers."""
		seen = set()
		layers = (layer,) if layer is not None else self.layers()
		for cell in self.cells_for_rect(rect):
			for i in layers:
				layer = self.grid[i]
				if cell in layer:
					for obj in layer[cell]:
						# Even if the object is in one of the searched cells, it might not actually intersect the requested rect
						if obj not in seen and obj.rect.colliderect(rect):
							seen.add(obj)
		return seen
	
	def objects(self):
		"""Returns the set of all objects on the grid."""
		seen = set()
		for layer in self.grid.values():
			for block in layer.values():
				seen.update(block)
		return seen
						
def get_class(name):
	"""Gets a class by its fully-qualified name, starting the search at the toplevel (sys.modules)."""
	first, *rest = name.split(".")
	cur = sys.modules[first]
	for part in rest:
		cur = getattr(cur, part)
	return cur
						
def load(config, callback):
	"""Loads a level from a dictionary of configuration parameters. callback will be called when
	the level is fully loaded.
	The required keys are:
	- "pool": the name of the resource pool that holds this level's resources
	- "object_path": a sequence containing the names of the modules where the level's objects are found
	- "objects": a sequence of sequences, each of which should start with the fully-qualified name
	of a class found in one of object_path's modules, followed by the arguments to be passed to that class's constructor.
	- "startpos": a mapping from character names (the names of their classes) to 3-tuples of their (x, y, layer) initial positions. The value for "default" is used if the character's name
	is not in the mapping. """
	level = Level(config["startpos"])
	# Import all referenced modules
	for name in config["object_path"]:
		__import__(name)
	def load_callback():
		# Each object definition is a tuple with the appropriate class and the instantiation arguments
		for objdef in config["objects"]:
			level.add(get_class(objdef[0])(*objdef[1:]))
		callback()
	resources.load(config["pool"], load_callback)
	return level

class Level:
	"""This class is used for coordinating the game's physics and drawing during a level.
	It also reads the sensor values for use by the player's collision routine."""
	
	game_over_sound = sound(rel("sfx/player/game_over.wav"), "global")
	act_clear_sound = sound(rel("sfx/end_tally/act_clear_s3k.wav"), "global")
	
	def __init__(self, spawn_positions = {}, background=Background(color=(222, 222, 222)), background_music=None, act_number=1, on_end=exit, bounding_box=None):
		"""Creates a new level object. The spawn positions, background, background music,
		act number (1 by default) and on-end trigger (exits the game by default) may be specified.
		- The spawn positions are given as a mapping from character names to 3-tuples of their initial positions (see level.load)
		- The background may be any instance of background.Background or a subclass. Defaults
		  to a solid light gray background.
		- The background music (if not None) should be specified as a tuple containing the intro
		- The bounding box should be specified as a pygame.Rect object describing the area
		within which the level is contained.
		and loop tracks' filenames."""
		self.grid = ObjectGrid()
		self.moving_objects = set()
		self.camera = Camera()
		self.background = background
		self.act_number = act_number
		self.on_end = on_end
		self.background.level = self
		self.background_music = background_music
		self.bounding_box = bounding_box
		self.spawn_positions = spawn_positions
		# The time the player has spent in the level
		self.timer = 0
		self.fader = None
		self.end_tally = None
		
	def add(self, obj):
		obj.level = self
		self.grid.add(obj)
		if isinstance(obj, MovingObject):
			self.moving_objects.add(obj)
		
	def remove(self, obj):
		self.grid.remove(obj)
		if isinstance(obj, MovingObject):
			self.moving_objects.remove(obj)
	
	@contextmanager
	def move(self, obj):
		"""Creates a context manager allowing the given object to be moved within its scope.
		This must be called whenever an object is moved, so that the level object can keep
		track of which cells the object is in.
		
		Example:
		with self.level.move(self):
			self.x += 4
		"""
		self.grid.remove(obj)
		yield
		self.grid.add(obj)
		
	def dump_objects(self):
		"""Returns a list of the constructor tuples for the level's objects."""
		return [obj.dump() for obj in self.grid.objects()]
		
	def place_player(self, player):
		"""Places the given player at the starting position specified for this level.
		If the player was dead, fades the level back in, resets its state variables,
		and those of all objects."""
		self.player = player
		player.level = self
		player.set_pos(*self.spawn_positions.get(type(player), self.spawn_positions["default"]))
		if self.player.mode == "dead":
			for obj in self.grid.objects():
				obj.reset()
			def remove_fader():
				self.fader = None
			self.fader.fade_in(on_return=remove_fader)
			self.camera.frozen = False
			self.camera.center(self.player)
		self.player.set_initial_state()
			
	
	def object_at(self, x, y, layer):
		"""Returns the first object found at (x, y) in the given layer, or None if none is found."""
		for obj in self.grid.objects_in_rect(Rect(x, y, 0, 0), layer = layer):
			return obj
	
	def draw(self, surface, rem_time):
		"""Draws the level's background, all objects and the player. The player is drawn behind any
		objects in the same layer."""
		rect = self.camera.interpolated_rect(rem_time)
		self.background.draw(surface, rect, rem_time)
		for i in self.grid.layers():
			for obj in self.grid.objects_in_rect(rect, layer = i):
				obj.draw(surface, rect, rem_time)
			if self.player.layer == i:
				self.player.draw(surface, rect, rem_time)
		draw_hud(surface, self.player.score, self.timer, self.player.rings, self.player.lives)
		if self.fader is not None:
			self.fader.draw(surface, rem_time)
		if self.end_tally is not None:
			self.end_tally.draw(surface)
	
	def start(self):
		"""Signals to the level object that gameplay has begun. This, for instance, allows it
		to being playing the background music."""
		if self.background_music is not None:
			music.play(*self.background_music)
	
	def update(self):
		"""Runs one iteration of the collision and physics processing for the level."""
		self.player.save_pos()
		self.camera.save_pos()
		self.handle_collision()
		for obj in self.grid.objects_in_rect(self.camera.rect) | self.moving_objects:
			obj.update()
			if isinstance(obj, PhysicsObject):
				self.handle_object_collision(obj)
		if self.player.mode != "dead" and self.bounding_box is not None and self.player.y > self.bounding_box.bottom:
			self.player.kill()
		self.player.update()
		self.check_death()
		self.camera.shift(self.player)
		self.background.update()
		if self.fader is not None:
			self.fader.update()
		if self.end_tally is not None:
			self.end_tally.update()
		if self.end_tally is None and self.player.mode != "dead":
			self.timer += 1
		
	def check_death(self):
		"""Checks if the player is dead and has already fallen below the edge of the screen. If so,
		fades to black."""
		if (self.player.mode == "dead" and self.player.y > self.camera.y + self.camera.height and self.player.y_speed > 0
		    and self.fader is None):
			def on_end_fade():
				# If the player has no more lives left, show Game Over screen, else respawn
				if self.player.lives == 0:
					self.runner.current_section = aux.GameOver()
					music.pause()
					get_sound(self.game_over_sound).play()
				else:
					self.place_player(self.player)
			self.fade_to_black(on_end_fade)
	
	def fade_to_black(self, on_end):
		"""Fades the screen to black, calling on_end when done.
		Do not use this during another fade-out."""
		self.fader = FadeToBlack(on_end=on_end)
			
	def handle_object_collision(self, obj):
		nearby_objects = self.grid.objects_in_rect(obj.collision_rect, layer=obj.layer)
		inf = float("inf")
		a = b = inf
		ld = rd = inf
		for n_obj in nearby_objects:
			# Objects can't collide with themselves
			if n_obj.ignore_physics or obj is n_obj:
				continue
			r = physics.right_sensor(obj, n_obj)
			if n_obj.left_solid and r != inf and obj.should_hit_left_wall(r):
				obj.on_right_wall_collision(n_obj)
				rd = min(rd, r)
			l = physics.left_sensor(obj, n_obj)
			if n_obj.right_solid and l != inf and obj.should_hit_right_wall(l):
				obj.on_left_wall_collision(n_obj)
				ld = min(ld, l)
		if isinstance(obj, GroundFollowingObject):
			obj.collide_walls(ld, rd)
		for n_obj in nearby_objects:
			if n_obj.ignore_physics or obj is n_obj:
				continue
			u = physics.up_sensor(obj, n_obj)
			if n_obj.bottom_solid and u != inf and obj.should_hit_ceiling(u):
				obj.on_ceiling_collision(n_obj)
			d = physics.down_sensor(obj, n_obj)
			if n_obj.top_solid and d != inf:
				if obj.should_hit_floor(d):
					obj.on_floor_collision(n_obj)
				if isinstance(obj, GroundFollowingObject):
					a_sensor = physics.quadrants["floor"].a
					b_sensor = physics.quadrants["floor"].b
					a = min(a, a_sensor(obj, n_obj))
					b = min(b, b_sensor(obj, n_obj))
		if isinstance(obj, GroundFollowingObject):
			obj.collide_ground(a, b, ld, rd)
	
	def handle_collision(self):
		"""Measures the distance along each active ground sensor to the nearest pixel of ground, and passes
		the resulting values to the player's collide method."""
		# The minimum distance along each sensor line
		inf = float("inf")
		values = [inf] * 13
		player = self.player
		nearby_objects = self.grid.objects_in_rect(player.collision_rect, layer = player.layer)
		# Do wall collision first (so that the player may be pushed out of walls before
		# ground measuresments are done, avoiding glitches where the player suddenly climbs
		# a vertical wall it should push against)
		for obj in nearby_objects:
			# The distances measured for the current object
			cvalues = [inf] * 13
			if obj.ignore_physics:
				if player.rect.colliderect(obj.rect):
					obj.on_collision(player)
				continue
			if obj.right_solid:
				cvalues[4] = player.quadrant.e(player, obj)
				if player.airborne:
					if player.quadrant.name != "air":
						print("Wrong quadrant when airborne: " + quadrant.name)
						print("Player mode: " + player.mode)
					cvalues[8] = player.quadrant.i(player, obj)
					cvalues[9] = player.quadrant.j(player, obj)
				else:
					cvalues[5] = player.quadrant.f(player, obj)
			if obj.left_solid:
				cvalues[6] = player.quadrant.g(player, obj)
				if player.airborne:
					cvalues[10] = player.quadrant.k(player, obj)
					cvalues[11] = player.quadrant.l(player, obj)
				else:
					cvalues[7] = player.quadrant.h(player, obj)
			left_wall = min(cvalues[4:6])
			right_wall = min(cvalues[6:8])
			# Let the player determine if there actually is a collision
			if left_wall != inf and player.should_hit_left_wall(left_wall, obj.x_speed):
				obj.on_left_wall_collision(player)
			if right_wall != inf and player.should_hit_right_wall(right_wall, obj.x_speed):
				obj.on_right_wall_collision(player)
			# Check if object is still solid after the player collides with it
			# (relevant for destructible objects)
			if not obj.ignore_physics:
				# Take the minimum values for each sensor
				for i in range(len(values)):
					values[i] = min(values[i], cvalues[i])
		player.collide_walls(*values)
		# After this correction, determine ground collision
		for obj in nearby_objects:
			if obj.ignore_physics:
				continue
			cvalues = [inf] * 13
			if obj.top_solid:
				cvalues[0] = player.quadrant.a(player, obj)
				cvalues[1] = player.quadrant.b(player, obj)
				# Ignore collision with the floor if the player is inside the object (which
				# is indicated by either vertical downards sensor yielding 0), such that
				# it can walk past ledges without being pushed to the top of them
				if cvalues[0] == 0:
					cvalues[0] = inf
				if cvalues[1] == 0:
					cvalues[1] = inf
			if player.airborne and obj.bottom_solid:
				cvalues[2] = player.quadrant.c(player, obj)
				cvalues[3] = player.quadrant.d(player, obj)
			if player.quadrant.name == "floor":
				cvalues[12] = player.quadrant.x(player, obj)
			
			floor = min(cvalues[0:2])
			ceiling = min(cvalues[2:4])
			# If the sensors detected this object, have the player determine if there actually has
			# been a collision. (This may not be the case, for example at the start of a jump)
			if floor != inf and player.should_hit_floor(floor, obj.y_speed):
				obj.on_floor_collision(player)
			if ceiling != inf and player.should_hit_ceiling(ceiling, obj.y_speed):
				obj.on_ceiling_collision(player)
			# Same deal as in the previous loop
			if not obj.ignore_physics:
				for i in range(len(values)):
					values[i] = min(values[i], cvalues[i])
		player.collide_ground(*values)
	
	def end(self):
		"""Ends the level and displays the score tally. Further calls to this method
		on the same level have no effect."""
		if self.end_tally is None:
			self.player.on_end_level()
			self.end_tally = EndOfLevelTally(self)
			music.pause()
			get_sound(self.act_clear_sound).play()
		
hud_vertical_space = 22
		
def draw_hud(surface, score, time, rings, lives):
	"""Draws the level HUD at the top left corner of the screen, displaying the time since
	the start of the level, number of rings, score and lives. Time should be given in frames."""
	font = text.fonts["classic"]
	seconds = time // 60
	minutes = seconds // 60
	seconds = seconds % 60
	font.draw_text("score {:06}".format(score), surface, 8, 8)
	font.draw_text("time {} {:02}".format(minutes, seconds), surface, 8, 8 + hud_vertical_space)
	font.draw_text("rings {}".format(rings), surface, 8, 8 + 2 * hud_vertical_space)
	font.draw_text("lives {}".format(lives), surface, 8, 8 + 3 * hud_vertical_space)
		
class FadeToBlack:
	"""This class provides a way of fading the screen to black, executing a callback
	when the fade completes, and optionally fading back in.
	
	duration is the time (in frames) that it takes to fade out (and in).
	on_end is a function that will be called when the fade to black completes."""
	def __init__(self, duration=60, on_end=lambda: None):
		# The fading to black is accomplished by drawing black rectangles of increasing opacity
		# over the screen (which is just the first argument to draw())
		self.counter = 0
		self.duration = duration
		self.fading_in = False
		self.on_end = on_end
		self.black_fill = None
	
	def update(self):
		self.counter += 1
		if self.counter == self.duration:
			self.on_end()
	
	def draw(self, surface, rem_time):
		if self.black_fill is None:
			black_fill = Surface(surface.get_size())
		# Change the opacity in the reverse direction when fading in
		step = self.counter + rem_time
		if self.fading_in:
			step = self.duration - step
		black_fill.set_alpha(int(step / self.duration * 255))
		surface.blit(black_fill, (0, 0))
		
	def fade_in(self, on_return=lambda: None):
		"""Causes the fader to fade the screen back in (to whatever would be on the screen below the black cover).
		If on_return is specified, it is called when the fade-in completes."""
		self.on_end = on_return
		self.counter = 0
		self.fading_in = True
		
def frames(minutes, seconds):
	return 60 * (60 * minutes + seconds)
		
def time_bonus(time):
	"""Returns the score bonus corresponding to the time (in frames) taken to clear the level."""
	if time <= frames(0, 59):
		return 50000
	elif time <= frames(1, 29):
		return 10000
	elif time <= frames(1, 59):
		return 5000
	elif time <= frames(2, 29):
		return 4000
	elif time <= frames(2, 59):
		return 3000
	elif time <= frames(3, 29):
		return 1000
	elif time <= frames(9, 58):
		return 100
	elif frames(9, 59) <= time < frames(10, 0):
		return 100000
	else:
		return 0

def ring_bonus(rings):
	"""Returns the score bonus corresponding to the amount of rings the player has collected."""
	return rings * 100

class EndOfLevelTally:
	# The amount of points tallied (from each bonus) for each frame of counting.
	counting_rate = 100
	# The number of frames the tally waits before disappearing, after it finishes tallying
	# the player's score
	wait_time = wait_counter = 180
	# The time the tally waits before starting (adjust according to the length of the Act Clear
	# jingle)
	start_wait_counter = 240
	
	tick_sound = sound(rel("sfx/end_tally/tick.wav"), "global")
	finish_sound = sound(rel("sfx/end_tally/ka_ching.wav"), "global")
	
	def __init__(self, level):
		self.level = level
		self.player = level.player
		self.time_bonus = time_bonus(level.timer)
		self.ring_bonus = ring_bonus(level.player.rings)
	
	def update(self):
		if self.start_wait_counter > 0:
			self.start_wait_counter -= 1
			return
		time_partial = min(self.counting_rate, self.time_bonus)
		ring_partial = min(self.counting_rate, self.ring_bonus)
		# If there are no more points to deduct, wait for a while and then call the
		# end-of-level handler
		if time_partial == 0 and ring_partial == 0:
			if self.wait_counter == 0:
				self.level.fade_to_black(self.level.on_end)
			else:
				if self.wait_counter == self.wait_time:
					get_sound(self.finish_sound).play()
			self.wait_counter -= 1
		else:
			self.time_bonus -= time_partial
			self.ring_bonus -= ring_partial
			self.player.score += time_partial + ring_partial
			get_sound(self.tick_sound).stop()
			get_sound(self.tick_sound).play()
	
	def draw(self, surface):
		font = text.fonts["classic"]
		font.draw_text("sonic has got through", surface, 110, 54)
		font.draw_text("   act {}".format(self.level.act_number), surface, 158, 54 + hud_vertical_space)
		
		font.draw_text("time     {}".format(self.time_bonus), surface, 110, 54 + 3 * hud_vertical_space)
		font.draw_text("rings    {}".format(self.ring_bonus), surface, 110, 54 + 4 * hud_vertical_space)
		
		font.draw_text("score    {}".format(self.player.score), surface, 110, 54 + 6 * hud_vertical_space)