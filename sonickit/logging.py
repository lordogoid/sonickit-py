"""This module provides a facility for logging several gameplay-related statistics. These
statistics can then be read out by accessing the counted_events and measurements variables
of this module.

The methods beggining with 'player_' record mostly counts of individual events; the counts for these events can be found under the same name (minus the initial 'player_') in counted_events,
except for those pertaining to monitors and enemies, which are keyed by the type of monitor or enemy in hit_monitors, skipped_monitors, hit_enemies and skipped_enemies.
"""

from collections import defaultdict
from pprint import pprint

counted_events = defaultdict(int)
hit_monitors = defaultdict(int)
killed_enemies = defaultdict(int)
damage_taken = defaultdict(int)
measurements = defaultdict(list)
game_time = 0

last_spindash_start = None

def player_jumped():
	counted_events["jumped"] += 1

def player_rolled():
	counted_events["rolled"] += 1

def player_began_spindash(game_time):
	"""Indicates that a spindash has begun. The spindash itself is only recorded when
	player_ended_spindash is called."""
	# Count number of revs for each spindash
	counted_events["rev_spindash"] = 1
	global last_spindash_start
	last_spindash_start = game_time

def player_charged_spindash():
	"""Indicates the player has tapped to charge their spindash."""
	counted_events["rev_spindash"] += 1

def player_ended_spindash(game_time):
	"""Indicates that a spindash has ended. Its duration in seconds and number of times the spindash was charged are stored as a tuple and added to measurements["spindash"]."""
	measurements["spindash"].append(((game_time - last_spindash_start) / 60, counted_events["rev_spindash"]))

def player_hit_monitor(monitor_type):
	hit_monitors[monitor_type] += 1

def player_killed_enemy(enemy_type):
	killed_enemies[enemy_type] += 1

def player_took_damage_from(thing_type):
	damage_taken[thing_type] += 1

def player_hit_moving_platform():
	counted_events["hit_moving_platform"] += 1

def bouncing_rings_spawned(num_rings):
	"""Records the spawning of num_rings bouncing rings."""
	counted_events["bouncing_rings_spawned"] += num_rings

def player_collected_bouncing_ring():
	counted_events["bouncing_rings_collected"] += 1

def player_died_damage():
	counted_events["player_died_damage"] += 1

def player_died_pit():
	counted_events["player_died_pit"] += 1

def measure_speed(speed):
	"""Records a speed value for the player. All recorded speeds can be found (in order of
	measurement) in measurements["speed"]."""
	measurements["speed"].append(speed)

def level_ended(time):
	"""Records the time it took to end the level."""
	global game_time
	game_time = time

def clear_log():
	"""Resets all logging counters and clears measurement lists."""
	counted_events.clear()
	measurements.clear()
	hit_monitors.clear()
	killed_enemies.clear()
	game_time = 0

def print_log_counters():
	"""Displays all event counters on the console."""
	pprint(counted_events)
	pprint(hit_monitors)
	pprint(killed_enemies)
	pprint(damage_taken)