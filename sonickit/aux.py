"""This module provides implementations of auxiliary screens, such as menus, that are not part
of the levels."""

from . import text

class GameOver:
	# For compatibility with Level
	def start(self):
		pass
	
	def draw(self, surface, rem_time):
		surface.fill((0, 0, 0))
		text.fonts["classic"].draw_text("game over", target=surface, x=surface.get_width() // 2 - 64, y=surface.get_height() // 2 - 11)
	
	def update(self):
		pass