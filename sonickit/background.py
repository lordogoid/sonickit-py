"""This module provides a basic implementation of a parallax-scrolling background. In such a
background, it is possible to define several layers, each of which scrolls at a different speed
to create a perception of depth."""

import math
from .resources import get_image

class Background:
    def __init__(self, color, layers=()):
        self.color = color
        self.layers = []
        for layer in layers:
            self.add_layer(layer)
    
    def add_layer(self, layer):
        self.layers.append(layer)
        layer.background = self
        
    # There could be animated layers which need to be updated
    def update(self):
        for layer in self.layers:
            layer.update()
    
    def draw(self, surface, camera_rect, rem_time):
        surface.fill(self.color)
        for layer in self.layers:
            layer.draw(surface, x_offset=camera_rect.left, y_offset=camera_rect.top)

class ImageLayer:
    """A background layer that simply draws a static image. The image can be tiled along the X
    or Y axes."""
    def __init__(self, initial_x, initial_y, image, relative_x_speed=0, relative_y_speed=0, repeat_x=False, repeat_y=False):
        """Creates a static background layer.
        
        Its initial position is specified by (initial_x, initial_y); these are absolute screen coordinates (in other words, not relative to the camera's position).
        x_relative_speed and y_relative_speed are the number of pixels the layer moves along the
        respective axes for each pixel the camera moves. These values may be negative, causing
        the layer to move in the opposite direction to the camera.
        repeat_x and repeat_y indicate whether the image is repeated along the X or Y axes."""
        self.initial_x = initial_x
        self.initial_y = initial_y
        self.image = image
        self.relative_x_speed = relative_x_speed
        self.relative_y_speed = relative_y_speed
        self.repeat_x = repeat_x
        self.repeat_y = repeat_y
    
    def update(self):
        pass
    
    def draw(self, surface, x_offset, y_offset):
        frame = get_image(self.image)
        x = self.initial_x + x_offset * self.relative_x_speed
        y = self.initial_y + y_offset * self.relative_y_speed
        # Compute the number of times we must draw the image (in case of repetition) to ensure
        # the screen is filled in the desired directions
        if self.repeat_x:
            while x > 0.1:
                x -= frame.get_width()
            nx = math.ceil((self.background.level.camera.width - x) / frame.get_width())
        else:
            nx = 1
        if self.repeat_y:
            while y > 0.1 and self.repeat_y:
                y -= frame.get_width()
            ny = math.ceil((self.background.level.camera.height - y) / frame.get_height())
        else:
            ny = 1
        for ix in range(nx):
            for iy in range(ny):
                surface.blit(frame, dest=(x + ix * frame.get_width(), y + iy * frame.get_width()))