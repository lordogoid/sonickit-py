"""This module is used for specifying the keys to assign to the game's controls; they can
be changed by assigning to one of the relevant attributes. These attributes are:
- left, right, up, and down (default to the corresponding arrow keys)
- jump (defaults to spacebar)
- action (defaults to X key)
- secondary action (defaults to C key)
"""

import pygame.locals as K

left = K.K_LEFT
right = K.K_RIGHT
up = K.K_UP
down = K.K_DOWN

jump = K.K_SPACE
action = K.K_x
secondary_action = K.K_c