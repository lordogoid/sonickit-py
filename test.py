from sonickit import Runner, level, store, builtins, boss, logging
import sonickit.resources as res
from sonickit.players.sonic import Sonic
from pprint import pprint

runner = Runner()

res.load_global()

clear = False

def callback():
	global clear
	clear = True
	
test_level = level.load(store.load(res.rel("testlevel/slz.py")), callback)
test_level.place_player(Sonic())

while not clear: pass

runner.current_section = test_level
runner.run()

logging.print_log_counters()