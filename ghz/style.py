from sonickit.resources import image as res_image, rel
from sonickit import background
from levelgen import terrain
from pygame import Surface, draw, image, SRCALPHA

# The offset at which the GHZ grass sits from the ground
GRASS_OFFSET = 8

class GHZPlatform(terrain.HeightMappedPlatform):
	"""A generic GHZ-style platform whose shape is determined by a height map."""
	def __init__(self, x, y, layer, height_map):
		super().__init__(x, y, layer, height_map, top_margin=GRASS_OFFSET)
		self.image = generate_sprite(height_map)

checkers_pattern = image.load(rel("checkers.png"))
grass_pattern = image.load(rel("grass.png"))

# The offset at which the GHZ grass sits from the ground
GRASS_OFFSET = 4

def generate_sprite(height_map):
	"""Generates a sprite for a GHZ-esque platform of the given shape."""
	sprite = Surface((height_map.width, height_map.maximum_height + GRASS_OFFSET), flags=SRCALPHA)
	terrain.tile(sprite, checkers_pattern)
	# Rotate through the columns of the grass pattern
	grass_i = 0
	for x in range(height_map.width):
		height = height_map(x)
		sprite.fill(color=(0, 0, 0, 0), rect=(x, 0, 1, height_map.maximum_height - height + GRASS_OFFSET))
		# (maximum_height - height + GRASS_OFFSET - GRASS_OFFSET) simplifies to the expression below
		sprite.blit(grass_pattern, dest=(x, height_map.maximum_height - height), area=(grass_i, 0, 1, grass_pattern.get_height()))
		grass_i = (grass_i + 1) % grass_pattern.get_width()
	return sprite

class GHZBackground(background.Background):
	upper_mountains = res_image(rel("background/upper_mountains.png"), "global")
	lower_mountains = res_image(rel("background/lower_mountains.png"), "global")
	
	def __init__(self):
		super().__init__(color=(0, 0, 255), layers=[
			background.ImageLayer(image=self.upper_mountains, initial_x=0, initial_y=60, repeat_x=True, relative_x_speed=-0.25, relative_y_speed=0.1),
			background.ImageLayer(image=self.lower_mountains, initial_x=0, initial_y=105, repeat_x=True, relative_x_speed=-0.6, relative_y_speed=0.1)
		])

background_music = (rel("ghz_bgm_intro.wav"), rel("ghz_bgm_loop.wav"))