import random
import math
from sonickit.players.sonic import Sonic
from sonickit import builtins
from sonickit import object as sk_object
from sonickit import resources as res
from sonickit.animation import StaticFrame, Animation
from ghz import style
from levelgen import terrain, structure, aux


DEFAULT_HEIGHT_STDDEV = 32
DEFAULT_WIDTH = 384

HOLE_WIDTH = 70
MIN_PLATFORM_WIDTH = 30

DEFAULT_QUARTER_PIPE_OFFSET = 32

class GHZStaticPlatform(sk_object.SpriteObject):
	animation = StaticFrame(res.image(res.rel("small_platform.png"), "global"))
	collision_mask_name = res.image(res.rel("small_platform_mask.png"), "global")

class GHZMovingPlatform(builtins.MovingPlatform):
	animation = GHZStaticPlatform.animation
	collision_mask_name = GHZStaticPlatform.collision_mask_name

class SegmentedObject(sk_object.Object):
	def __init__(self, x, y, layer, num_segments):
		super().__init__(x, y, layer)
		self.num_segments = num_segments
		assert num_segments > 0
		# Compute the height of the object dynamically from the number of segments
		self.width = self.top_sprite().get_width()
		segment_sprite = self.sprite_for_segment(0)
		self.height = self.top_sprite().get_height() + segment_sprite.get_height() * num_segments
		self.width_diff = self.top_sprite().get_width() - segment_sprite.get_width()
		assert self.width_diff >= 0
	
	def sprite_for_segment(self, row): pass
	
	def draw(self, surface, camera, rem_time):
		base_x = self.rect.left - camera.x
		base_y = self.rect.bottom - camera.y
		segment_x = base_x + self.width_diff / 2
		for i in range(self.num_segments):
			sprite = self.sprite_for_segment(i)
			surface.blit(sprite, (segment_x, base_y - sprite.get_height() * (i + 1)))
		return base_x, base_y

class Palmtree(SegmentedObject):
	top_sprite_name = res.image(res.rel("palmtree_top.png"), "global")
	segment_sprite_name = res.image(res.rel("palmtree_segment.png"), "global")
	
	def sprite_for_segment(self, row):
		return res.get_image(self.segment_sprite_name)
	
	def top_sprite(self):
		return res.get_image(self.top_sprite_name)
	
	def draw(self, surface, camera, rem_time):
		base_x, base_y = super().draw(surface, camera, rem_time)
		top_sprite = self.top_sprite()
		surface.blit(top_sprite, (base_x, base_y - self.sprite_for_segment(0).get_height() * self.num_segments - top_sprite.get_height()))
		
from pygame import transform

rotated_sunflowers = {}

class Sunflower(SegmentedObject):
	low_segment_sprite_names = res.image([res.rel("sunflower_low_segment_a.png"), res.rel("sunflower_low_segment_b.png")], "global")
	mid_segment_sprite_names = res.image([res.rel("sunflower_mid_segment_a.png"), res.rel("sunflower_mid_segment_b.png")], "global")
	top_sprite_name = res.image(res.rel("sunflower_top_2.png"), "global")
	
	period = 38 * 8
	
	def __init__(self, x, y, layer, num_segments=3):
		# This determines which way the leaves of the sunflower will face
		self.orientation = random.randint(0, 1)
		self.counter = 0
		super().__init__(x, y, layer, num_segments)
	
	def sprite_for_segment(self, row):
		sprite_set = self.low_segment_sprite_names if row == 0 else self.mid_segment_sprite_names
		return res.get_image(sprite_set[self.orientation])
	
	def top_sprite(self):
		return res.get_image(self.top_sprite_name)
	
	def draw(self, surface, camera, rem_time):
		base_x, base_y = super().draw(surface, camera, rem_time)
		index = self.counter % self.period
		phase = self.counter / self.period * 2 * math.pi
		if index not in rotated_sunflowers:
			rotated_sunflowers[index] = transform.rotate(self.top_sprite(), math.degrees(phase))
		rotated = rotated_sunflowers[index]
		surface.blit(rotated, (self.x - camera.x - rotated.get_width() / 2, base_y - self.sprite_for_segment(0).get_height() * self.num_segments - (self.top_sprite().get_height() + rotated.get_height()) / 2 + 4))
	
	def update(self):
		self.counter += 1
		
TOTEMPOLE_WINGS_CHANCE = 0.35

class TotemPole(SegmentedObject):
	sprites = res.image([res.rel("totem_{}.png".format(name)) for name in ("squint", "o", "smile", "frown", "deepfrown")], "global")
	wing_sprite_name = res.image(res.rel("totempole_wings.png"), "global")
	
	def __init__(self, x, y, layer, num_segments=3):
		# Choose num_segments random sprites
		self.choices = [res.get_image(random.choice(self.sprites)) for i in range(num_segments)]
		# The topmost totem is considered the "top" sprite
		super().__init__(x, y, layer, num_segments)
		self.wing_sprite = res.get_image(self.wing_sprite_name)
		self.wing_diff = (self.wing_sprite.get_width() - self.top_sprite().get_width()) / 2
		# Add wings with some probability
		self.has_wings = random.random() < TOTEMPOLE_WINGS_CHANCE
	
	def sprite_for_segment(self, row):
		return self.choices[row]
	
	def top_sprite(self):
		return self.choices[-1]
	
	def draw(self, surface, camera, rem_time):
		base_x, base_y = super().draw(surface, camera, rem_time)
		if self.has_wings:
			surface.blit(self.wing_sprite, (base_x - self.wing_diff, base_y - self.top_sprite().get_height() * self.num_segments))

class StretchingFlower(sk_object.Object):
	stem_sprite_name = res.image(res.rel("stretching_flower_stem.png"), "global")
	flower_name = res.image(res.rel("stretching_flower_top.png"), "global")
	
	switching_interval = 180
	flower_shift_time = 45
	max_flower_shift = 10
	
	def __init__(self, x, y, layer):
		self.flower_sprite = res.get_image(self.flower_name)
		self.stem_sprite = res.get_image(self.stem_sprite_name)
		self.width = self.flower_sprite.get_width()
		self.height = self.stem_sprite.get_height() + self.flower_sprite.get_height()
		self.switch_counter = 0
		# Goes back and forth between 0 and max_flower_shift
		self.shift_counter = 0
		# When this is -1, the flower rises; when it's 1, it goes down.
		self.shift_direction = 1
		super().__init__(x, y, layer)
	
	def draw(self, surface, camera, rem_time):
		base_y = self.rect.bottom - camera.y
		screen_x = self.x - camera.x
		stem_y = base_y - self.stem_sprite.get_height()
		surface.blit(self.stem_sprite, (screen_x - self.stem_sprite.get_width() / 2, stem_y))
		# Draw the flower at its current position
		flower_shift = self.shift_counter / self.flower_shift_time * self.max_flower_shift
		surface.blit(self.flower_sprite, (screen_x - self.flower_sprite.get_width() / 2, stem_y - self.flower_sprite.get_height() + flower_shift))
	
	def update(self):
		self.switch_counter += 1
		if self.switch_counter >= self.switching_interval:
			self.shift_counter += self.shift_direction
			if not 0 <= self.shift_counter <= self.flower_shift_time:
				self.shift_counter = max(min(self.shift_counter, self.flower_shift_time), 0)
				self.shift_direction *= -1
				self.switch_counter = 0
			

def build_basic_ground(level, area, has_neighbors, exits, ground_rings_chance=0.5):
	has_left, has_right, has_up, has_down = has_neighbors
	left_exit, right_exit, up_exit, down_exit = exits
	
	
	
	# Determine whether the platform already has other platforms to the left or the right;
	# if so, then place its right and left ends (respectively) so they line up with those.
	if has_left and left_exit is not None:
		starting_height = area.height - left_exit[1]
	else:
		starting_height = random.gauss(area.height / 2, DEFAULT_HEIGHT_STDDEV)
	if has_right and right_exit is not None:
		final_height = area.height - right_exit[1]
	else:
		final_height = random.gauss(area.height / 2, DEFAULT_HEIGHT_STDDEV)
		
	# If there is only an exit upwards and towards only one of the sides, it makes sense to build
	# a quarter-pipe (and it demarcates paths more clearly)
	if has_up and not has_down and has_left ^ has_right:
		if up_exit is None:
			up_exit = (0, area.width - DEFAULT_QUARTER_PIPE_OFFSET) if has_left else (DEFAULT_QUARTER_PIPE_OFFSET, area.width)
		height_map = terrain.QuarterPipeHeightMap(
		  starting_height=starting_height if has_left else final_height,
		  horizontal_offset=(area.width - up_exit[1]) if has_left else up_exit[0],
		  width=area.width,
		  height=area.height,
		  point_left=has_left
		)
		platform = style.GHZPlatform(x=area.left + area.width / 2, y=area.bottom - height_map.maximum_height / 2, layer=1, height_map=height_map)
		level.add(platform)
		left_exit = (0, area.height - starting_height) if has_left else None
		right_exit = (0, area.height - final_height) if has_right else None
		return [platform], (left_exit, right_exit, up_exit, None)
		
	height_map = terrain.HeightMap(starting_height=starting_height, height_iterator=lambda h: final_height, max_segments=1, width=area.width, flat_weight=0, ramp_weight=0, slope_weight=1.5)
	
	platforms = []
	
	if has_down:
		# Place a hole at a position where it fits entirely within the block's width
		if down_exit is None:
			down_exit_width = random.uniform(HOLE_WIDTH * 3/4, HOLE_WIDTH)
			down_exit_x = random.uniform(down_exit_width / 2, area.width - down_exit_width / 2)
			down_exit = (down_exit_x - down_exit_width / 2, down_exit_x + down_exit_width / 2)
		# If the hole is close enough to the edges, do not build the side of the platform
		# nearest to that edge
		right_width = area.width - down_exit[1]
		if right_width >= MIN_PLATFORM_WIDTH:
			right_height_map = terrain.HeightMap(starting_height=final_height, height_iterator=lambda h: h, max_segments=1, width=right_width, flat_weight=1, ramp_weight=0, slope_weight=0)
			platforms.append(style.GHZPlatform(x=area.left + down_exit[1] + right_width / 2, y=area.bottom - right_height_map.maximum_height / 2, layer=1, height_map=right_height_map))
		left_width = down_exit[0]
		if left_width >= MIN_PLATFORM_WIDTH:
			left_height_map = terrain.HeightMap(starting_height=starting_height, height_iterator=lambda h: h, max_segments=1, width=left_width, flat_weight=1, ramp_weight=0, slope_weight=0)
			platforms.append(style.GHZPlatform(x=area.left + left_width / 2, y=area.bottom - left_height_map.maximum_height / 2, layer=1, height_map=left_height_map))
			
	else:
		platforms.append(style.GHZPlatform(x=area.left + height_map.width / 2, y=area.bottom - height_map.maximum_height / 2, layer=1, height_map=height_map))
		
	for platform in platforms:
		level.add(platform)
	
	# Add a spring if we have an upwards exit
	if has_up:
		spring_x = area.left + sum(up_exit) / 2 if up_exit is not None else None
		spring = builtins.UpYellowSpring(0, 0, 1)
		spring.launch_speed = 12.5
		# Look for a platform that the spring can be placed on. If there isn't one, create
		# one on the spot
		if spring_x is not None:
			for platform in platforms:
				if platform.rect.left < spring_x < platform.rect.right:
					platform.place_object_atop(spring, x=spring_x)
					break
			else:
				platform_y = area.bottom - area.height / 2
				extra_platform = GHZStaticPlatform(x=spring_x, y=platform_y, layer=1)
				spring.x = spring_x
				spring.y = platform_y - extra_platform.rect.height / 2 - spring.rect.height / 2 + 4
				level.add(extra_platform)
		else:
			random.choice(platforms).place_object_atop(spring)
		level.add(spring)
		if up_exit is None:
			up_exit = (spring.x - spring.width - area.left, spring.x + spring.width - area.left)
		
	if random.random() < ground_rings_chance:
		# Space out the ground rings a bit more than in the ring clusters
		ring_width = builtins.Ring(0,0,0).rect.width + 2 * INTER_RING_GAP
		for platform in platforms:
			num_rings = math.floor(platform.width / ring_width)
			for i in range(num_rings):
				ring = builtins.Ring(x=0, y=0, layer=1)
				platform.place_object_atop(ring, x=platform.rect.left + ring_width * i, y_offset=INTER_RING_GAP)
				level.add(ring)
	
	add_decoration(level, platforms)
	
	return platforms, ((0, area.height - platform.height_map.starting_height), (0, area.height - platform.height_map.final_height), up_exit, down_exit)

MAX_DECORATION_ELEMENTS = 3
DECORATION_SEGMENTS = {Palmtree: (2, 9), Sunflower: (2, 4), TotemPole: (2, 4)}
DECORATION_TYPES = (Palmtree, Sunflower, TotemPole, StretchingFlower)

def add_decoration(level, platforms):
	num_elements = random.randint(1, MAX_DECORATION_ELEMENTS)
	for i in range(num_elements):
		bauble_type = random.choice(DECORATION_TYPES)
		if issubclass(bauble_type, SegmentedObject):
			bauble = bauble_type(x=0, y=0, layer=random.choice((0, 2)), num_segments=random.randint(*DECORATION_SEGMENTS[bauble_type]))
		else:
			bauble = bauble_type(x=0, y=0, layer=random.choice((0, 2)))
		random.choice(platforms).place_object_atop(bauble)
		level.add(bauble)

from sonickit import boss

def build_start_node(level, area, has_neighbors, has_adjacent, exits):
	platforms, exits = build_basic_ground(level, area, has_neighbors, exits)
	platform = platforms[0]
	# This Sonic object is only used as a dummy, to allow use of place_object_atop to determine
	# the starting position.
	start = Sonic()
	platform.place_object_atop(start)
	level.spawn_positions["default"] = (start.x, start.y, 1)
	return exits

def build_interstitial_node(level, area, has_neighbors, has_adjacent, exits, ground_rings_chance=0.75):
	platforms, exits = build_basic_ground(level, area, has_neighbors, exits, ground_rings_chance)
	return exits

def build_final_node(level, area, has_neighbors, has_adjacent, exits, boss_chance=0, boss_difficulty_level=2):
	platforms, exits = build_basic_ground(level, area, has_neighbors, exits)
	# Add an end sign or a boss to the level
	if random.random() < boss_chance:
		make_boss(level, platforms, area, boss_difficulty_level)
	else:
		sign = builtins.EndSign(x=0, y=0, layer=1)
		platforms[-1].place_object_atop(sign)
		level.add(sign)
	return exits

# The ratio by which the speed of boss elements is increased for each extra point of difficulty.
SPEED_BOOST_PER_POINT = 1.15
	
BOSS_ELEMENTS = (boss.BounceUpDown, boss.HoverLeftRight, boss.Drill, boss.Hammer, boss.WreckingBall)

def make_boss(level, platforms, area, difficulty_level):
	"""Creates a boss with approximately the indicated difficulty level. The scale is as follows:
	- 0 creates a trivial boss (no movement or weapons)
	- 3 creates a moderately-difficult one with 1-2 movement patterns and 1-2 weapons
	- 5 creates a very hard boss with lots of patterns and weapons
	
	Levels beyond the number of total elements that can be added to the boss will instead
	increase its speed."""
	platform = max(platforms, key=lambda platform: platform.width)
	new_boss = boss.Boss(x=0, y=0, layer=1)
	platform.place_object_atop(new_boss, y_offset=65)
	new_boss.set_initial_position()
	level.add(new_boss)
	# Pick as many base elements as possible before resorting to speedups
	num_base_elements = min(len(BOSS_ELEMENTS), difficulty_level)
	elements = []
	for element_type in random.sample(BOSS_ELEMENTS, num_base_elements):
		if element_type is boss.Drill or element_type is boss.Hammer:
			element = element_type()
			new_boss.attach(element)
		elif element_type is boss.WreckingBall:
			element = element_type(radius=80, angular_velocity=math.pi / 60 * random.choice((1, -1)))
			# Spin the wrecking ball clockwise or counter-clockwise at random
			new_boss.attach(element)
		elif element_type is boss.BounceUpDown:
			# The boss only goes as far down as the height of the platform directly below its
			# starting position
			element = element_type(area.bottom - platform.height_map(new_boss.x - platform.rect.left))
			new_boss.add_movement_pattern(element)
		elif element_type is boss.HoverLeftRight:
			element = element_type(area.left, area.right)
			new_boss.add_movement_pattern(element)
		elements.append(element)
	# Speed up one element for each of the remaining diffculty points
	for i in range(max(0, difficulty_level - num_base_elements)):
		target = random.choice(elements)
		# Drills and bounces cannot be sped up
		if isinstance(target, (boss.Drill, boss.BounceUpDown)):
			continue
		elif isinstance(target, boss.WreckingBall):
			target.angular_velocity *= SPEED_BOOST_PER_POINT
		elif isinstance(target, boss.Hammer):
			# NOTE: this will only affect strikes after the first. Done this way to avoid
			# depending on an internal Hammer variable.
			target.strike_interval /= SPEED_BOOST_PER_POINT
		elif isinstance(target, boss.HoverLeftRight):
			target.speed_up(SPEED_BOOST_PER_POINT)
	return new_boss
	
MAX_RING_ROWS = 4
INTER_RING_GAP = 6
DELTA_CHANCE = 0.8

def make_ring_arrangement(area, start_y):
	"""Generates an arrangement of rings resembling those seen in the original Sonic games,
	such that it fits into the specified rectangle and starts at y = start_y.
	Returns an iterable containing the rings."""
	r = builtins.Ring(0, 0, 0).rect
	ring_x_interval = r.width + INTER_RING_GAP
	y = start_y
	x = random.uniform(area.left, area.left + area.width / 2)
	num_rows = random.randint(1, MAX_RING_ROWS)
	rings_per_row = random.randint(1, 5)
	for j in range(num_rows):
		initial_x = x
		for i in range(rings_per_row):
			yield builtins.Ring(x, y, layer=1)
			x += ring_x_interval
		# Increase, decrease or mantain the number of rings per row, at random
		rings_per_row_delta = random.choice((0, 1, -1))
		if rings_per_row_delta == 0:
			y += r.height + INTER_RING_GAP
			x = initial_x
		else:
			# If the number of rings changes, shift the next row slightly rightwards or leftwards
			# (so the new row is centered relative to the existing one)
			y += r.height + INTER_RING_GAP / 2
			x = initial_x - rings_per_row_delta / 2 * ring_x_interval
			if random.random() < DELTA_CHANCE:
				rings_per_row += rings_per_row_delta

def build_bonus_area(level, area, has_neighbors, has_adjacent, exits, shield_weight=1, ring_weight=1, life_weight=1, ring_cluster_weight=1, max_bonuses=3):
	platforms, exits = build_basic_ground(level, area, has_neighbors, exits, ground_rings_chance=0)
	num_bonuses = random.randint(1, max_bonuses)
	choice = aux.WeightedChoice([(builtins.ShieldMonitor, shield_weight),
                             (builtins.RingMonitor, ring_weight),
						     (builtins.LifeMonitor, life_weight),
						     ("ring_cluster", ring_cluster_weight)])
	for i in range(num_bonuses):
		bonus_type = choice.choice()
		if bonus_type == "ring_cluster":
			for ring in make_ring_arrangement(area, start_y=random.uniform(area.top, area.top + area.height / 2)):
				level.add(ring)
		else:
			monitor = bonus_type(x=0, y=0, layer=1)
			random.choice(platforms).place_object_atop(monitor)
			level.add(monitor)
	return exits
	
MOVING_PLATFORM_OFFSET = 40
UP_PLATFORM_EXCESS = 130
MAX_ENEMIES = 3
# The enemy types that can be placed on regular ground, and the distances at which they should
# be placed from it.
ENEMY_TYPES = (builtins.BuzzBomber, builtins.Chopper, builtins.Motobug)
Y_ENEMY_OFFSETS = {builtins.BuzzBomber: 100, builtins.Chopper: -60, builtins.Motobug: 0}

LOOP_END_HEIGHT = 64
LOOP_SIZE = 256

def build_challenge_area(level, area, has_neighbors, has_adjacent, exits, moving_platforms_chance=0.5, enemies_chance=0.5, loop_chance=0.15):
	"""Builds a challenge area, with a chance of inserting enemies and a chance of replacing
	the normal ground with moving platforms."""
	has_left, has_right, has_up, has_down = has_neighbors
	left_exit, right_exit, up_exit, down_exit = exits
	# Loops only run left to right, and the height of both entrances must (ideally) be the same
	# If it is not possible to achieve the latter, align the loop with the highest entrance
	# It is also only possible to build a loop is there is no block directly above it
	# (even if without a connection between them)
	if not has_down and not has_up and has_left and has_right and not has_adjacent[2] and random.random() < loop_chance:
		if left_exit is None and right_exit is None:
			left_exit = right_exit = (0, area.height - LOOP_END_HEIGHT)
		elif left_exit is None:
			left_exit = right_exit
		elif right_exit is None:
			right_exit = left_exit
		left_height = area.height - left_exit[1]
		right_height = area.height - right_exit[1]
		height_difference = max(left_height, right_height) - LOOP_END_HEIGHT
		# Add the two halves of the loop...
		position = (area.left + area.width / 2, area.top + area.height / 2 - height_difference)
		level.add(builtins.LoopFront(*position, layer=1))
		level.add(builtins.LoopBack(*position, layer=0))
		# and the 4 path swappers (one at the middle of the loop, another at the end;
		# each must be present in both layers 0 and 1)
		mid_swapper_params = {"x": position[0], "y": position[1] - 94, "layer_forward": 0, "layer_backward": 1}
		level.add(builtins.PathSwapper(layer=1, **mid_swapper_params))
		level.add(builtins.PathSwapper(layer=0, **mid_swapper_params))
		end_swapper_params = {"x": area.right - 8, "y": position[1] + 75, "layer_forward": 1, "layer_backward": 0}
		extra_swapper_params = {"x": area.right + 8, "y": position[1] + 40, "height": 90, "layer_forward": 1, "layer_backward": 0}
		level.add(builtins.PathSwapper(layer=1, **end_swapper_params))
		level.add(builtins.PathSwapper(layer=0, **end_swapper_params))
		level.add(builtins.PathSwapper(layer=1, **extra_swapper_params))
		level.add(builtins.PathSwapper(layer=0, **extra_swapper_params))
		return left_exit, right_exit, None, None
		
	use_moving_platforms = random.random() < moving_platforms_chance
	if use_moving_platforms:
		# If there are exits to both the left and the right, create a horizontal platform across
		if has_left and has_right:
			if left_exit is None:
				left_exit = (0, random.uniform(0, area.height))
			if right_exit is None:
				right_exit = (0, random.uniform(0, area.height))
			level.add(GHZMovingPlatform(x=area.left + MOVING_PLATFORM_OFFSET, y=area.top + left_exit[1], end_x=area.right - MOVING_PLATFORM_OFFSET, end_y=area.top + right_exit[1], layer=1, period=300))
		# If there is an exit upwards, create a platform that goes up
		if has_up:
			if up_exit is None:
				midpoint = random.uniform(MOVING_PLATFORM_OFFSET, area.width - MOVING_PLATFORM_OFFSET)
				up_exit = (midpoint - MOVING_PLATFORM_OFFSET, midpoint + MOVING_PLATFORM_OFFSET)
			else:
				midpoint = sum(up_exit) / 2
			x = area.left + midpoint
			level.add(GHZMovingPlatform(x=x, y=area.bottom, end_x=x, end_y=area.top - UP_PLATFORM_EXCESS, layer=1))
		# If there's an exit downwards, there's no need to create a special platform, as the
		# player can simply drop down past the moving platforms.
		exits = (left_exit, right_exit, up_exit, down_exit)
	else:
		platforms, exits = build_basic_ground(level, area, has_neighbors, exits)
	if random.random() < enemies_chance:
		num_enemies = random.randint(1, MAX_ENEMIES)
		if use_moving_platforms:
			# If only moving platforms are used, then Motobugs and Choppers don't make much sense
			for i in range(num_enemies):
				level.add(builtins.BuzzBomber(x=random.uniform(area.left, area.right), y=random.uniform(area.top, area.bottom), layer=1))
		else:
			for i in range(num_enemies):
				enemy_type = random.choice(ENEMY_TYPES)
				enemy = enemy_type(x=0, y=0, layer=1)
				random.choice(platforms).place_object_atop(enemy, y_offset=Y_ENEMY_OFFSETS[enemy_type])
				enemy.set_initial_position()
				level.add(enemy)
	return exits
	
implementation_table = {
	structure.StartPoint: (build_start_node, {}),
	structure.InterstitialNode: (build_interstitial_node, {"ground_rings_chance": 0.75}),
	structure.EndPoint: (build_final_node, {"boss_difficulty_level": 3, "boss_chance": 1}),
	structure.BonusArea: (build_bonus_area, {"shield_weight": 1, "ring_weight": 4, "life_weight": 0.5, "ring_cluster_weight": 2, "max_bonuses": 3}),
	structure.ChallengeArea: (build_challenge_area, {})
}